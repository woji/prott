/*
EditClientDlg to add new project client
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';

import useMediaQuery       from '@mui/material/useMediaQuery';

import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'


//form field value change
export const ffValChange = (name, val) => (
  {this[name]: val}
);


export default function EditDlg(props){

  console.log("EditDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  //const recDefaults = {id:"", data:{name:"", desc:"", color:""}}



  useEffect(() => {
    // on props change
    //console.log("EditTaskDlg.propsChanged()");
    //console.log(props.client)
    if(props.item)
      setState({item:props.item,mode:"replace"})
    else
      setState({item:props.itemDefaults,mode:"add"})

  }, [props])



  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      item:itemDefaults,
      mode:"add",
    }
  )



  const hndlChange = (e, propId) => {
    //console.log(e);

    propId = (propId === undefined) ? e.target.id : propId
    //console.log(propId);
    hndlPropChange(e.target.value, propId)
  }



  const hndlPropChange = (propVal, propId) => {
    const item = state.item

    if(propId.startsWith("data"))
      item.data[propId.split(".")[1]] = propVal

    else
      item[propId] = propVal

    setState({item})
  }



  const modRecLbl = (state.mode === "add") ? t("Add") : t("Update")

  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="edit-item"
      maxWidth='xs'
      sx={{minWidth:480}}
    >
    <DialogTitle>{`${modRecLbl} ${props.itemName}`}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Box component="span">{t("enter ")+props.itemName+t(" data")}</Box>
        </DialogContentText>
        {
          (!props || !props.children)
            ? null
            : (!props.children.length)
              ? <props.children.type {...props.children.props} doSomething={doSomething} {...props}>{props.children}</props.children.type>
              : props.children.map((child, key) =>
                React.cloneElement(child, {...props, key, onChange={hndlChange}}))
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={e => {setState({client:recDefaults,mode:"add"});props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={e => {props.onSave(state.client,state.mode)}}>{modRecLbl}</Button>
      </DialogActions>
    </Dialog>
  );

}
