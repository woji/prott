/*
EditClient to add/edit new project client
created by:wojkowski.free@gmail.com
created at:2022-03-26
*/
import React, { useState, useEffect, useReducer } from "react";

import TextField           from '@mui/material/TextField';

import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'
import EditDlg, {ffValChange}             from 'fragment/EditDlg'



export default function EditClient(props){

  console.log("EditClient()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()


  const updFF = () => {

  }


  return (
    <EditDlg
      itemName="client"
      itemDefaults = {{id:"", data:{name:"", desc:"", color:""}}}
      item={props.item}
      open={props.open}
      onClose={props.onClose}
      onSave={props.onSave}
    >
      <TextField
        autoFocus={(state.mode === "add")}
        disabled={(state.mode !== "add")}
        margin="dense"
        id="id"
        label={t("client id/code")}
        fullWidth
        variant="outlined"
        value={state.client.id}
        onChange={updFF}
      />
      <TextField
        autoFocus={(state.mode !== "add")}
        margin="dense"
        id="data.name"
        label={t("name")}
        fullWidth
        variant="outlined"
        value={state.client.data.name}
        onChange={hndlChange}
      />
      <TextField
        margin="dense"
        id="data.desc"
        label={t("description")}
        fullWidth
        variant="outlined"
        value={state.client.data.desc}
        onChange={hndlChange}
      />
      <TextField
        margin="dense"
        id="data.color"
        label={t("color")}
        fullWidth
        variant="outlined"
        value={state.client.data.color}
        onChange={hndlChange}
      />
    </EditDlg>
  );

}
