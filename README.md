# PROject Time Tracker

freely inspired by [React-TimeTracker](https://github.com/shijuvar/React-TimeTracker)

with much simplified stack to react+sqlite3

see [database structure](https://gitlab.com/woji/prott/-/blob/main/PROTT.sql) to understand how it works

there are 3 base roles in app:
- guest, can only view data, can view public projects
- user, can fill timesheet, can have team role (eg. developer, analyst, tester,...)
- admin, can manage users, projects

in addition to these roles, user can get team role (see ROLE table), eg developer, tester,... and these roles are then paired with project tasks. User is assigned to project not to task, so role match is used to display proper tasks in timesheet.

some screens

### main application console screen

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-console.png" width="400" alt="console">

### project groups (client) list

rather then client it is group of projects :). Each client card contains info about:
- num of project in this group,
- total num of tasks
- total num of assigned users
- last update of timesheet on any project

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-client-list.png" width="400" alt="add client">

### add new client

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-add-client.png" width="400" alt="add client">

### project list

each project card contains info about:
- planned start & end date of project
- total estimated (planed) hours on all project tasks
- already spent time on tasks

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-project-list.png" width="400" alt="logo">

### add project

project is somehow timebounded but its endTime is not used, it is just informative value

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-add-project.png" width="400" alt="addl project">

### task list

each task card contains info about:
- planned start & end date of task
- planned estimated task hours

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-task-list.png" width="400" alt="logo">

### add task to project

project task is timebounded and has estimated workday time that is adjusted from dates to workdays only. Task also contains resource ratio where manager can assign employee allocation, eg. 1 = 1 employed works on task, 1.1 = one and 1/10... this allows to plan resources better and create project burndown chart.

Task also defines team role for which will be available in timesheet, only users assigned to these roles can see such a tasks in timesheet.

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-add-task.png" width="400" alt="add task">

### add team member

each team member can be assigned to app role (guest, user, admin) and also to team roles (user defined). team roles are used with tasks and timesheet, see tasks.

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-add-user.png" width="400" alt="add employee">

### assign project to employee

in PROTT project are assigned to team members, not tasks. While it looks weird, I decided to keep this idea from react-timetracker. It makes whole app very lightweight and simple.

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-assign-project.png" width="400" alt="logo">

### fill Timesheet

fill timesheet is made as much comfortable as possible as it is great annoyance for anybody :)

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-fill-timesheet.png" width="400" alt="fill week timesheet">

### edit Timesheet day

this is why each day opens dialog with predefined values, easy to change or enhance

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-timesheet-change-mdh.png" width="400" alt="fill week timesheet">

### users activity on project report

report displays user activity within past 2 months.

- Blue - all activities
- green - productive (paid) activities
- red - non productive activities

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-users-report.png" width="400" alt="user activity report">

### project activity report

report displays project activities within past 2 months

<img src="https://gitlab.com/woji/PROTT/-/raw/main/screenshoots/prott-project-report.png" width="400" alt="user activity report">
