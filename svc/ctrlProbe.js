/*
wojkowski.free
2022-03-12

test/probe controller
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();



/*
kontrola pripojeni na db
*/
exports.testDb = function(req, res) {
  _trace("testDb");

  //var sqlite3 = require('sqlite3').verbose();
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  const row = db.prepare('SELECT * FROM sqlite_schema ')
  console.log(row.sql);

  row.finalize()
  db.close()

  var dummy = {
    stat   : "0",
    msg    : "sqlite3 ok :)",
    tStamp : new Date().toISOString(),
  }
  res.json(dummy);
};



/*
testovaci funkce pro overeni zda API zije
*/
exports.ping = function(req, res) {
  _trace("ping");

  //res.send(err);
  var maketa = {
    stat   : "0",
    msg    : "node express jede :)",
    tStamp : new Date().toISOString(),
  }
  res.json(maketa);
};
