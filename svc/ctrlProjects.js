/*
wojkowski.free
2022-03-09

client mgmt controller
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const aSqlite3 = require('./a-sqlite3');
const trace = require('./trace');

/*
URI:
http://192.168.56.8:8003/API/projects
type:get
*/
exports.getProjectList = async function(req, res) {
  trace.log("getProjectList()");

  var clientId = req.params.clientId;

  var retVal={
    stat    : "0",
    msg     : "",
    tStamp  : new Date().toISOString(),
  }

  var sqls = [
    {
      resultId:"records",
      sql     : `
select p.PROJECT_ID as id, p.PROJECT_NAME as name, p.PROJECT_IS_BILLABLE as billable,
  p.PROJECT_CLIENT_ID as clientId, p.PROJECT_VISIBILITY as visibility,
  p.PROJECT_DATA as data, p.CREATED_AT as startDate, p.ENDS_AT as endDate,
  ifnull(tt.projectAllocTime,0) as totalAllocTime, ifnull(tt.count,0) as taskCount,
  ifnull(wt.projectWorkTime,0) as totalWorkTime
from PROJECT p
left join (
  select sum(TASK_ESTIMATE_TIME * TASK_RESOURCE_ALLOCATION) as projectAllocTime, count(*) as count, TASK_PROJECT_ID
  from TASK
  group by TASK_PROJECT_ID
) tt
on p.PROJECT_ID = tt.TASK_PROJECT_ID
left join (
  select sum(TIMESHEET_HOURS) as projectWorkTime, TASK_PROJECT_ID
  from TASK t
  inner join TIMESHEET ts
  on t.TASK_ID = ts.TIMESHEET_TASK_ID
  group by t.TASK_PROJECT_ID
) wt
on p.PROJECT_ID = wt.TASK_PROJECT_ID
where PROJECT_CLIENT_ID = '${clientId}'
`}
]
  //trace.log("run sql:"+sql);

  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)

  //trace.log(`open db`);
  //var db = await aaSqlite3.open(dbfile);


  for (let i = 0; i < sqls.length; i++) {
    var cmd = sqls[i]

    try{
      //trace.log(`open db n:${i}`);
      var db = await aSqlite3.open(dbfile);

      //trace.log(cmd.sql)
      trace.log(`i:${i}`)
      const rows = await db.all(cmd.sql, []);
      //trace.log(`close db n:${i}`);
      await db.close()

      rows.forEach((row) => {
        //trace.log(row.data);
        //convert json string to json before sent
        row.data = JSON.parse(row.data)
        //billable from int to bool
        if (row.billable !== undefined) row.billable = (row.billable === 0) ? false : true
      });

      //trace.log(cmd.resultId);
      retVal[cmd.resultId] = rows
      //trace.log(`${cmd.resultId}:${JSON.stringify(rows)}`)
    }
    catch(e){
      trace.log("err:"+e.message);
      res.status(400).json(
        {
          stat   : "-1",
          msg    : e.message,
          tStamp : new Date().toISOString(),
        });
      return
    }
  }
  res.json(retVal);
  //await aaSqlite3.close()






  /*
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    trace.log("close db")
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(
        {
          stat   : "-1",
          msg    : err.message,
          tStamp : new Date().toISOString(),
        }
      );
      return
    }

    //convert json string to json before sent
    rows.forEach((row) => {
      //trace.log(row.project);
      row.data = JSON.parse(row.data)
      //billable from int to bool
      row.billable = (row.billable === 0) ? false : true
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
  */
}



exports.modProject = function(req, res) {
  trace.log("addProject()");

  //todo:validate users role !! admin role requred

  //var postData = JSON.stringify(req.body);
  trace.log(JSON.stringify(req.body));

  var rec = req.body.project

  //todo:begin tran here

  var prjData = JSON.stringify(rec.data)
  var sqls = []

  if(req.body.op === "replace" || req.body.op === "delete"){

    sqls.push(`
delete from PROJECT
where PROJECT_ID = '${rec.id}'
      `
    )
  }

  if(req.body.op !== "delete"){
    sqls.push(`
insert into PROJECT
values (
  '${rec.id}','${rec.name}',${rec.billable}
  ,'${rec.clientId}',${rec.visibility}
  ,'${prjData}','${rec.startDate}','${rec.endDate}'
)
`
    )
  }

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.serialize(() => {
    // Queries here will be serialized.
    var errMsg = ""

    sqls.forEach((sql, i) => {
      db.run(sql, (e,d) => {
        if(e) {errMsg+='\n'+e.message;trace.log(`err:${errMsg}`)};

        if((i+1) === sqls.length)
          res.json( (errMsg === "")
          ? {
              stat   : "0",
              msg    : req.body.op+" ok",
              tStamp : new Date().toISOString(),
            }
          : {
              stat   : "-1",
              msg    : errMsg,
              tStamp : new Date().toISOString(),
            })
      })
    });
    db.close();

  });
}
