/*
wojkowski.free
2022-03-11

user mgmt controller, task assignment
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const trace = require('./trace');

/*
URI:
http://192.168.56.8:8003/API/users/:userId/assignments
type:get
*/
exports.getAssignmentList = function(req, res) {
  trace.log("getAssignmentList");

  var userId = req.params.userId;
  //var filter=req.query["filter"];

  var sql = `
select p.PROJECT_ID as projectId, case when USER_ID is null then 0 else 1 end as assigned,
  case when used.PROJECT_ID is null then 0 else 1 end as inUse
from PROJECT p
left join PROJECT_ASSIGN_USER pa
on p.PROJECT_ID = pa.PROJECT_ID
and pa.USER_ID = '${userId}'
left join (
  select distinct p.PROJECT_ID
  from PROJECT p
  inner join TASK t
  on p.PROJECT_ID = t.TASK_PROJECT_ID
  inner join TIMESHEET ts
  on ts.TIMESHEET_TASK_ID = t.TASK_ID
  where ts.TIMESHEET_USER_ID = '${userId}'
) used
on pa.PROJECT_ID = used.PROJECT_ID
where '${new Date().toISOString()}' between p.CREATED_AT and p.ENDS_AT
`

  //trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(vytvoritChybovouZpravu(err.message));
      return
    }

    rows.forEach((row) => {
      //billable from int to bool
      row.inUse = (row.inUse === 0) ? false : true
      row.assigned = (row.assigned === 0) ? false : true
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
}



exports.modAssignment = function(req, res) {
  trace.log("modAssignment()");

  //todo:validate users role !! admin role requred
  trace.log(JSON.stringify(req.body));

  switch(req.body.op){
  case "add":
    addAssignment(req, res)
    break;
  case "delete":
    deleteAssignment(req, res)
    break;
  }

}



function addAssignment(req, res){

  var rec = req.body.assignment

  //todo:begin tran here

  /*
  var sql=`
  delete from PROJECT_ASSIGN_USER
  `
  trace.log("open db");
  var db=new sqlite3.Database('PROTT.sqlite');
  trace.log(sql);
  db.run(sql);
  */

  var userId = rec.userId

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  //loop process async sql!!
  rec.projects.forEach( (projectId, i) => {
    var sql = `
insert into PROJECT_ASSIGN_USER
values (
  '${projectId}','${userId}','${req.body.tStamp}'
)
`
    //trace.log(sql);

    db.run(sql,(err) => {
      if (err) {
        db.close();
        res.json(_vytvoritChybovouZpravu(err.message));
        return
      }
    });

    if((i+1) === rec.projects.length){
      //all sql processed ok
      db.close();
      res.json({
        stat   : "0",
        msg    : "record added ok",
        tStamp : new Date().toISOString(),
      });
    }
  });
}



function deleteAssignment(req, res){

    var rec = req.body.assignment

    //todo:begin tran here

    /*
    var sql=`
    delete from PROJECT_ASSIGN_USER
    `
    trace.log("open db");
    var db=new sqlite3.Database('PROTT.sqlite');
    trace.log(sql);
    db.run(sql);
    */

    var userId = rec.userId

    trace.log("open db");
    var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
    //trace.log(dbfile)
    var db = new sqlite3.Database(dbfile);

    //loop process async sql!!
    rec.projects.forEach( (projectId, i) => {
      var sql = `
delete from PROJECT_ASSIGN_USER
where PROJECT_ID = '${projectId}' and USER_ID = '${userId}'
`
      //trace.log(sql);

      db.run(sql,(err) => {
        if (err) {
          db.close();
          res.json(_vytvoritChybovouZpravu(err.message));
          return
        }
      });

      if((i+1) === rec.projects.length){
        //all sql processed ok
        db.close();
        res.json({
          stat   : "0",
          msg    : "record removed ok",
          tStamp : new Date().toISOString(),
        });
      }
    });
}



/*
URI:
http://192.168.56.8:8003/API/allprojects
type:get

!!! not used !!!!
*/
exports.getAllProjectList = function(req, res) {
  trace.log("getAllProjectList()");

  var sql = `
select PROJECT_ID as projectId, PROJECT_NAME as name
from PROJECT
where '${new Date().toISOString()}' between CREATED_AT and ENDS_AT
`
  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  //trace.log("sql:"+sql);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
}



function _vytvoritChybovouZpravu(msg){
  return {
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  };
}
