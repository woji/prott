/*
wojkowski.free
2022-03-21

user auth mgmt controller
*/

'use strict';

//const sqlite3 = require('sqlite3').verbose();
const aSqlite3 = require('./a-sqlite3');
const trace = require('./trace');
const cookieParser = require('cookie-parser');
const DatePlus = require('./DatePlus');

/*
URI:
http://192.168.56.8:8003/API/doBasicAuth
type:get
https://stackoverflow.com/questions/23616371/basic-http-authentication-with-node-and-express-4
*/
exports.doBasicAuth = async function(req, res) {
  trace.log("doBasicAuth()");

  //res.set('WWW-Authenticate', 'Basic realm="401"') // change this
  //res.status(401).send('Authentication required.') // custom message
  //return

  //const auth = {login: 'yourlogin', password: 'yourpassword'} // change this

  // parse login and password from headers
  //const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
  //const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')
  const [login, password] = parseLoginAndPwd(req)

  trace.log(login);trace.log(password);

  if(req.headers.authorization && login===""){
    trace.log("received empty login, force guest mode");
    res.json(
    {
      stat   : "0",
      msg    : "login empty, continue as quest",
      tStamp : DatePlus.addHours(new Date(),1).toISOString(),
      user:{
        id:"guest",firstName:"Guest",lastName:"User",employeeId:"0000000",
        email:"todo@set.mail",roles:0,data:null,
        login:"guest"
      },
    });
    return
  }

  var sql = `
select USER_ID as id, USER_FIRST_NAME as firstName, USER_LAST_NAME as lastName,
USER_EMPLOYEE_ID as employeeId, USER_EMAIL as email, USER_ROLES as roles,
USER_DATA as data, USER_LOGIN as login, USER_PASSWORD as password
from USER
where USER_LOGIN = '${login}'
`

  //trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = await aSqlite3.open(dbfile);
  
  try{
    const r = await db.get(sql, []);

    await db.close()

    //trace.log(JSON.stringify(result));
    if(!r || r.password !== password){
      trace.log("not found");
      res.set('WWW-Authenticate', 'Basic realm="401"') // change this
      res.status(401).send('Authentication required.') // custom message
      return
    }
    //login&password ok
    //remove password from response !!
    delete r.password

    let options = {
      maxAge: 24 * 60 * 60 * 1000, // would expire after 1 day (24hours)
      httpOnly: true, // The cookie only accessible by the web server
      //signed: true // Indicates if the cookie should be signed
    }

    let msg = {
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      user   : r
    }

    // Set cookie
    //res.cookie('prott-auth', msg, options).json(msg);
    //or without cookie
    res.json(msg);
  }
  catch(e){
    trace.log("err:"+e.message);
    res.status(400).json(
      {
        stat   : "-1",
        msg    : e.message,
        tStamp : new Date().toISOString(),
      });
    return
  }

/*
  var sql = `
select USER_ID as id, USER_FIRST_NAME as firstName, USER_LAST_NAME as lastName,
USER_EMPLOYEE_ID as employeeId, USER_EMAIL as email, USER_ROLES as roles,
USER_DATA as data, USER_LOGIN as login, 'enter new password' as password
from USER
where USER_LOGIN = '${login}' and USER_PASSWORD = '${password}'
`
  trace.log("sql:"+sql);

  trace.log("open db");
  var db = new sqlite3.Database('PROTT.sqlite');

  db.get(sql, [], (err, row) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.status(400).json(
        {
          stat   : "-1",
          msg    : err.message,
          tStamp : new Date().toISOString(),
        });
      return
    }
    else if (row === undefined){
      // Access denied...
      res.set('WWW-Authenticate', 'Basic realm="401"') // change this
      res.status(401).send('Authentication required.') // custom message
    }
    else {
      let options = {
        maxAge: 24 * 60 * 60 * 1000, // would expire after 1 day (24hours)
        httpOnly: true, // The cookie only accessible by the web server
        //signed: true // Indicates if the cookie should be signed
      }

      let msg = {
        stat   : "0",
        msg    : "",
        tStamp : new Date().toISOString(),
        user   : row
      }

      // Set cookie
      res.cookie('prott-auth', msg, options).json(msg);
    }
  });
  */
}



function parseLoginAndPwd(req){
  trace.log(req.headers.authorization)
  // parse login and password from headers
   const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
   const strauth = Buffer.from(b64auth, 'base64').toString()
   trace.log(strauth)
   const splitIndex = strauth.indexOf(':')
   //const login = strauth.substring(0, splitIndex)
   //const password = strauth.substring(splitIndex + 1)

   // using shorter regex by @adabru
   // const [_, login, password] = strauth.match(/(.*?):(.*)/) || []

   return [
     strauth.substring(0, splitIndex),
     strauth.substring(splitIndex + 1)
   ]

}
