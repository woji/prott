/*
wojkowski.free
2022-03-09

client mgmt controller
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const aSqlite3 = require('./a-sqlite3');
const trace = require('./trace');

/*
URI:
http://192.168.56.8:8003/API/clients
type:get
*/
exports.getClientList = async function(req, res) {
  trace.log("getClientlist()");

  var retVal={
    stat    : "0",
    msg     : "",
    tStamp  : new Date().toISOString(),
  }

  var sqls = [
    {
      resultId:"records",
      sql     : `
select c.CLIENT_ID as id, c.CLIENT_DATA as data,
 ifnull(prj.count,0) as projectCount, ifnull(tsk.count,0) as taskCount,
 ifnull(usr.count,0) as userCount, ifnull(upd.lastDate, c.CREATED_AT) as lastUpdated
from CLIENT c
left join (
  select count(*) as count, PROJECT_CLIENT_ID
  from PROJECT
  group by PROJECT_CLIENT_ID
) prj
on c.CLIENT_ID = prj.PROJECT_CLIENT_ID
left join (
  select count(*) as count, p.PROJECT_CLIENT_ID
  from PROJECT p
  inner join TASK t
  on p.PROJECT_ID = t.TASK_PROJECT_ID
  group by PROJECT_CLIENT_ID
) tsk
on c.CLIENT_ID = tsk.PROJECT_CLIENT_ID
left join (
  select count(*) as count, p.PROJECT_CLIENT_ID
  from PROJECT p
  inner join PROJECT_ASSIGN_USER pa
  on p.PROJECT_ID = pa.PROJECT_ID
  group by PROJECT_CLIENT_ID
) usr
on c.CLIENT_ID = usr.PROJECT_CLIENT_ID
left join (
  select max(TIMESHEET_DATE) as lastDate, p.PROJECT_CLIENT_ID
  from PROJECT p
  inner join TASK t
  on p.PROJECT_ID = t.TASK_PROJECT_ID
  inner join TIMESHEET ts
  on t.TASK_ID = ts.TIMESHEET_TASK_ID
  group by PROJECT_CLIENT_ID
) upd
on c.CLIENT_ID = upd.PROJECT_CLIENT_ID
`}
]
  //trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'

  for (let i = 0; i < sqls.length; i++) {
    var cmd = sqls[i]

    try{
      //trace.log(`open db n:${i}`);
      var db = await aSqlite3.open(dbfile);

      //trace.log(cmd.sql)
      trace.log(`i:${i}`)
      const rows = await db.all(cmd.sql, []);
      //trace.log(`close db n:${i}`);
      await db.close()

      rows.forEach((row) => {
        //trace.log(row.data);
        //convert json string to json before sent
        row.data = JSON.parse(row.data)
        //billable from int to bool
        if (row.billable !== undefined) row.billable = (row.billable === 0) ? false : true
      });

      //trace.log(cmd.resultId);
      retVal[cmd.resultId] = rows
      //trace.log(`${cmd.resultId}:${JSON.stringify(rows)}`)
    }
    catch(e){
      trace.log("err:"+e.message);
      res.status(400).json(
        {
          stat   : "-1",
          msg    : e.message,
          tStamp : new Date().toISOString(),
        });
      return
    }
  }
  res.json(retVal);



  //trace.log(dbfile)
  /*
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }

    //convert json string to json before sent
    rows.forEach((row) => {
      //trace.log(row.client);
      row.data = JSON.parse(row.data)
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
  */
}



exports.modClient = function(req, res) {
  trace.log("modClient()");

  //todo:validate users role !! admin role requred

  //var postData = JSON.stringify(req.body);
  trace.log(JSON.stringify(req.body));

  //todo:begin tran here

  var clientData = JSON.stringify(req.body.client.data)
  var rec = req.body.client

  var sqls = []

  if(req.body.op === "replace" || req.body.op === "delete"){

    sqls.push(`
delete from CLIENT
where CLIENT_ID = '${rec.id}'
`
    )
  }

  if(req.body.op !== "delete"){
    sqls.push(`
insert into CLIENT values ('${req.body.client.id}','${clientData}','${req.body.tStamp}')
`
    )
  }

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.serialize(() => {
    // Queries here will be serialized.
    var errMsg = ""

    sqls.forEach((sql, i) => {
      //trace.log("sql:"+sql);
      db.run(sql, (e,d) => {
        if(e) {errMsg+='\n'+e.message;trace.log(`err:${errMsg}`)};

        if((i+1) === sqls.length)
          res.json( (errMsg === "")
          ? {
              stat   : "0",
              msg    : req.body.op+" ok",
              tStamp : new Date().toISOString(),
            }
          : {
              stat   : "-1",
              msg    : errMsg,
              tStamp : new Date().toISOString(),
            })
      })
    });
    db.close();

  });

}



function _vytvoritChybovouZpravu(msg){
  return {
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  };
}
