/*
2022-02-23
wojkowski.free

inspired byß
https://www.codementor.io/@olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd

server.js implements basic http handler settings (port, routing, parser, paralelism,...)
*/

var express = require('express'),
  app = express(),
  cors = require('cors')
  port = process.env.PORT || 8002,
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//dev environment, remove in release
app.use(cors({
  origin: 'http://192.168.56.8:8002',
  credentials:true
}));


//https://expressjs.com/en/starter/static-files.html
//map compiled SPA frontend UI to backend express site
//thus prottuidev docker service becomes development version only :)
app.use('/ui', express.static('/home/node/ui'))

var routes = require('./routes'); //importing routes
routes(app); //register all routes

app.listen(port);

console.log('PROTT dbSqlite JSON RESTful API server started on: ' + port);
