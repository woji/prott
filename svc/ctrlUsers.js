/*
wojkowski.free
2022-03-09

client mgmt controller
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const trace = require('./trace');
const DatePlus = require('./DatePlus');

/*
URI:
http://192.168.56.8:8003/API/clients
type:get
*/
exports.getUserList = function(req, res) {
  trace.log("getUserList");

  var userId=req.params["userId"];

  var sql = `
select u.USER_ID as id, USER_FIRST_NAME as firstName, USER_LAST_NAME as lastName,
  USER_EMPLOYEE_ID as employeeId, USER_EMAIL as email, USER_ROLES as roles,
  USER_DATA as data, USER_LOGIN as login, 'enter new password' as password,
  case when usg.USER_ID is null then 0 else 1 end as isUsed
from USER u
left join (
  select TIMESHEET_USER_ID as USER_ID
  from TIMESHEET
  group by TIMESHEET_USER_ID
  union
  select USER_ID
  from PROJECT_ASSIGN_USER
  group by USER_ID
) usg
on u.USER_ID = usg.USER_ID
`

  if( (userId || "") !== "")
    sql += `where u.USER_ID = '${userId}'
`

  //trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }

    //convert json string to json before sent
    rows.forEach((row) => {
      //trace.log(row.user);
      if(row.data) row.data = JSON.parse(row.data)
      row.isUsed = (row.isUsed === 0) ? false : true
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
}



exports.modUser = function(req, res) {
  trace.log("modUser()");

  //todo:validate users role !! admin role requred

  //var postData = JSON.stringify(req.body);
  trace.log(JSON.stringify(req.body));

  var sqls = []
  var rec = req.body.user
  //todo:begin tran here

  var userData = (rec.data) ? JSON.stringify(rec.data) : null

  if(req.body.op === "replace" || req.body.op === "delete"){

    sqls.push(`
delete from USER
where USER_ID = '${rec.id}'
`
    )
  }

  if(req.body.op !== "delete"){
    sqls.push(`
insert into USER
values (
  '${rec.id}','${rec.firstName}','${rec.lastName}','${rec.employeeId}','${rec.email}',${rec.roles},
  '${userData}','${rec.login}','${rec.password}','${rec.authToken}','${req.body.tStamp}'
)
  `
    )
  }
  //run all sqls ordered and dont close conn between commands (in future comimt/rollback transaction)
  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.serialize(() => {
  // Queries here will be serialized.

  //db.run(sqls[0], (e,d) => {return e ? _outErr(res,e.message) : null})
  //  .run(sqls[1], (e,d) => {return e ? _outErr(res,e.message) : null})
  //  .close();
  var errMsg = ""

  sqls.forEach((sql, i) => {
    //trace.log(sql)
    db.run(sql, (e,d) => {
      if(e) {errMsg+='\n'+e.message;trace.log(`err:${errMsg}`)};

      if((i+1) === sqls.length)
        res.json( (errMsg === "")
        ? {
            stat   : "0",
            msg    : req.body.op+" ok",
            tStamp : new Date().toISOString(),
          }
        : {
            stat   : "-1",
            msg    : errMsg,
            tStamp : new Date().toISOString(),
          })
      })
    });
    db.close();
  });
}



exports.getUserRoles = function(req, res) {
  trace.log("getUserRoles");

  var userId=req.params["userId"];

  if(userId==="guest"){
    //guest has only quest role
    res.json({
      stat   : "0",
      msg    : "",
      tStamp : DatePlus.addHours(new Date(),1).toISOString(),
      roles   : ["guest"]
    });
    return
  }

  var sql = `
select r.ROLE_NAME as role
from USER u
inner join ROLE r
on u.USER_ROLES & r.ROLE_ID > 0
where u.USER_ID = '${userId}'
`
  //trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }

    //convert records to array
    var retVal=[]
    //guest is always present
    retVal.push("guest")

    rows.forEach((row) => {
      retVal.push(row.role)
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      roles  : retVal
    });
  });

}



function _vytvoritChybovouZpravu(msg){
  return {
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  };
}
