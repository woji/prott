/*
wojkowski.free
2022-03-09

Task mgmt controller
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const trace = require('./trace');

var responseUsed = false

/*
URI:
http://192.168.56.8:8003/API/clients/:clientId/projects/:projectId/tasks
type:get
*/
exports.getTaskList = function(req, res) {
  trace.log("getTaskList()");

  var projectId = req.params.projectId;

  var sql = `
select TASK_ID as id, TASK_NAME as name, TASK_PROJECT_ID as projectId,
 TASK_ROLES as roles, TASK_DATA as data,
 TASK_ESTIMATE_TIME as estimateTime, TASK_RESOURCE_ALLOCATION as resourceRate,
CREATED_AT as startDate, ENDS_AT as endDate, ifnull(ts.taskWorkTime,0) as totalWorkTime
from TASK t
left join (
  select sum(TIMESHEET_HOURS) as taskWorkTime, TIMESHEET_TASK_ID
  from TIMESHEET
  group by TIMESHEET_TASK_ID
) ts
on t.TASK_ID = TIMESHEET_TASK_ID
where TASK_PROJECT_ID = '${projectId}'
`
  trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err)
      return _outErr(res,err.message)

    //convert json string to json before sent
    rows.forEach((row) => {
      //trace.log(row.data);
      row.data = JSON.parse(row.data)
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
}



exports.modTask = function(req, res) {
  trace.log("addTask()");

  //todo:validate users role !! admin role requred

  //var postData = JSON.stringify(req.body);
  trace.log(JSON.stringify(req.body));

  var rec = req.body.project
  var sqls = []

  //todo:begin tran here

  var taskData = JSON.stringify(rec.data)

  if(req.body.op === "replace" || req.body.op === "delete"){

    sqls.push(`
delete from TASK
where TASK_ID = '${rec.id}'
`
      )
  }


  if(req.body.op !== "delete"){
    sqls.push(`
insert into TASK
values (
  '${rec.id}','${rec.name}','${rec.projectId}','${rec.roles}',
  '${taskData}','${rec.estimateTime}',${rec.resourceRate},'${rec.startDate}','${rec.endDate}'
)
  `
    )
  }
  //trace.log(sql);

  //run all sqls ordered and dont close conn between commands (in future comimt/rollback transaction)
  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.serialize(() => {
    // Queries here will be serialized.

    //db.run(sqls[0], (e,d) => {return e ? _outErr(res,e.message) : null})
    //  .run(sqls[1], (e,d) => {return e ? _outErr(res,e.message) : null})
    //  .close();
    var errMsg = ""

    sqls.forEach((sql, i) => {
      db.run(sql, (e,d) => {
        if(e) {errMsg+='\n'+e.message;trace.log(`err:${errMsg}`)};

        if((i+1) === sqls.length)
          res.json( (errMsg === "")
          ? {
              stat   : "0",
              msg    : req.body.op+" ok",
              tStamp : new Date().toISOString(),
            }
          : {
              stat   : "-1",
              msg    : errMsg,
              tStamp : new Date().toISOString(),
            })
      })
    });
    db.close();

  });

}



function _outErr(res,msg){
  trace.log(msg)
  res.json({
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  })
}
