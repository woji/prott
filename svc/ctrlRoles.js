/*
wojkowski.free
2022-03-09

client mgmt controller
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const trace = require('./trace');
//https://stackabuse.com/get-query-strings-and-parameters-in-express-js/
//const url = require('url');

/*
URI:
http://192.168.56.8:8003/API/roles
type:get
*/
exports.getRoleList = function(req, res) {
  trace.log("getRoleList");

  //const qry = url.parse(req.url, true).query;
  //console.log(qry);

  var minRole = 0, maxRole = 2147483648 //2^31

  if(req.query.type === "project")
    minRole = 4

  var sql = `
select ROLE_ID as id, ROLE_NAME as name
from ROLE
where ROLE_ID between ${minRole} and ${maxRole}
order by 1
`
  trace.log("run sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      trace.log("err:"+err.message);
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });

}



function _vytvoritChybovouZpravu(msg){
  return {
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  };
}
