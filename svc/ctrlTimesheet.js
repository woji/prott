/*
wojkowski.free
2022-02-23

kontroler pro ...
*/

'use strict';

const sqlite3 = require('sqlite3').verbose();
const trace = require('./trace');
const DatePlus = require('./DatePlus');

//todo:work with locales, week can start on Fri, Sat, Sun, Mon !!
/*
function getDateOfWeek(w, y) {
  var date = new Date(y, 0, (1 + (w - 1) * 7)); // Elle's method
  date.setDate(date.getDate() + (1 - date.getDay())); // 0 - Sunday, 1 - Monday etc
  return date
}
*/

/*
URI:
http://192.168.56.8:8003/API/TS/2022/01/1000380
type:POST
*/
exports.getUserWeekTimeSheet = function(req, res) {
  trace.log("getUserWeekTimeSheet()");

  var year=parseInt(req.params["year"]);
  var weekNo=parseInt(req.params["week"]);
  var userId=req.params["userId"];
  var loc=req.query["loc"]; //locale eg. cs-CZ

  trace.log(`browser locale:${loc}`)

  var dr = DatePlus.getWeekDateRange(year, weekNo, loc)

  var ssql = "", sep = "", cn1 = " as TIMESHEET_DATE ", cn2=" as link"

  trace.log(dr.start.toISOString());
  trace.log(dr.end.toISOString());

  var day = new Date(dr.start)

  while(day <= dr.end) {
    //trace.log(day)
    ssql += `${sep}select '${day.toISOString()}'${cn1},1${cn2}`
    sep = " union "; cn1 = ""; cn2 = ""
    day.setDate(day.getDate() + 1);
  }
  //trace.log(ssql);
  //var retVal

  var sql=`
select t.PROJECT_IS_BILLABLE as billable, t.TASK_ID as taskId, t.PROJECT_NAME as projectName,
  t.TASK_NAME as taskName, strftime(ts.TIMESHEET_DATE,'%w') as weekMD,
  d.TIMESHEET_DATE as dateMD, IFNULL(ts.TIMESHEET_HOURS,0) as manHours,
  ts.TIMESHEET_STATUS as state, t.startDate, t.endDate
from (${ssql}) d
inner join (
  select 1 as link, p.PROJECT_NAME, t.TASK_NAME, t.TASK_ID, p.PROJECT_IS_BILLABLE,
    t.CREATED_AT as startDate, t.ENDS_AT as endDate
  from PROJECT p
  inner join TASK t
  on t.TASK_PROJECT_ID = p.PROJECT_ID
  inner join PROJECT_ASSIGN_USER pa
  on p.PROJECT_ID = pa.PROJECT_ID
  and pa.USER_ID = '${userId}'
  where '${new Date().toISOString()}' between t.CREATED_AT and t.ENDS_AT
) t
on d.link = t.link
left join (
  select TIMESHEET_TASK_ID, TIMESHEET_DATE, TIMESHEET_HOURS, TIMESHEET_STATUS
  from TIMESHEET ts
  where ts.TIMESHEET_USER_ID = '${userId}'
  and TIMESHEET_DATE between '${dr.start.toISOString()}' and '${dr.end.toISOString()}'
) ts
on d.TIMESHEET_DATE = ts.TIMESHEET_DATE
and t.TASK_ID = ts.TIMESHEET_TASK_ID
order by 1 desc, 2, 6
`
  //trace.log("sql:"+sql);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.all(sql, [], (err, rows) => {
    db.close();
    if(err) {
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }

    //add rowid to each record
    //not supported by sqlite
    rows.forEach((row,ix) => {
      //console.log(row.name);
      row["rowid"]=ix
    });

    res.json({
      stat   : "0",
      msg    : "",
      tStamp : new Date().toISOString(),
      records   : rows
    });
  });
}



exports.saveUserManDay = function(req, res) {
  trace.log("saveUserManDay()");

  var year=req.params["year"];
  var weekNo=req.params["week"];
  var userId=req.params["userId"];

  trace.log(JSON.stringify(req.body));

  var rec = req.body.record

  //todo:begin tran here

/*
  var sql=`
delete from timesheet ts
where strftime(ts.timesheet_date,'%Y') = ${year}
and strftime(ts.timesheet_date,'%W') = ${weekNo}
and ts.timesheet_user_id = '${userId}'
`
*/

  var sql1=`
delete from TIMESHEET
where TIMESHEET_DATE = '${rec.dateMD}'
and TIMESHEET_TASK_ID = '${rec.taskId}'
and TIMESHEET_USER_ID = '${userId}'
`
  //trace.log("sql:"+sql1);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db = new sqlite3.Database(dbfile);

  db.run(sql1,(err) => {
    db.close();
    if (err) {
      retVal=_vytvoritChybovouZpravu(err.message);
      res.json(retVal);
      return
    }
  });

  if(rec.manHours === 0){
    res.json({
      stat   : "0",
      msg    : "success",
      tStamp : new Date().toISOString(),
    });
    return
  }

var sql2=`
  insert into TIMESHEET
  (TIMESHEET_USER_ID,TIMESHEET_TASK_ID,TIMESHEET_DATE,TIMESHEET_HOURS,TIMESHEET_STATUS)
  values ( ? , ? , ? , ? , ? )
`
  //trace.log("sql:"+sql2);

  trace.log("open db");
  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'
  //trace.log(dbfile)
  var db2 = new sqlite3.Database(dbfile);

  //test sqlite prepare API
  var stmt = db2.prepare(sql2);

  stmt.run(userId,rec.taskId,rec.dateMD,rec.manHours,'submit',err => {
    if (err) {
      db2.close();
      res.json(_vytvoritChybovouZpravu(err.message));
      return
    }
  });

  stmt.finalize();
  db2.close();

  res.json({
    stat   : "0",
    msg    : "record updated",
    tStamp : new Date().toISOString(),
  });
}



function _vytvoritChybovouZpravu(msg){
  return {
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  };
}
