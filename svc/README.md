# SVC - app backend

backend is nodejs server with simple sqlite db

current state:
- backend serves db content and accepts data from app

missing functions:
- all handlers require users right check (now only UI block modify functions, backend accepts direct call from any user)
- sqlite db add foreign keys
