/*
wojkowski.free
2022-03-15

user reporting controller
*/

'use strict';

const aSqlite3 = require('./a-sqlite3');
const trace = require('./trace');
const DatePlus = require('./DatePlus');

/*
users activity within last 2 months
URI:
http://192.168.56.8:8003/API/reports/projects/:projectId
type:get
*/
exports.getReportData = async function(req, res) {
  trace.log("TaskReport.getReportData");

  //var projectId = req.params.projectId;
  var projectId = req.params.projectId;
  var taskId = req.params.taskId;
  var locale = req.query.loc //eg. cs-CZ

  const drTLM = DatePlus.getDateRange("thisAndLastMonth")
  const drLW = DatePlus.getDateRange("lastWeek", locale)
  const nWeek = DatePlus.getWeekNumber(drLW.start)
  const drTM = DatePlus.getDateRange("thisMonth")
  const drLM = DatePlus.getDateRange("lastMonth")

  //trace.log(drTLM.start);trace.log(drTLM.end);
  //trace.log(drLW.start);trace.log(drLW.end);
  //trace.log(drTM.start);trace.log(drTM.end);
  //trace.log(drLM.start);trace.log(drLM.end);

  var retVal={
    stat    : "0",
    msg     : "",
    tStamp  : new Date().toISOString(),
  }

  //fill date ranges
  retVal["usedDates"] = {
    thisAndLastMonth:drTLM,
    lastWeek:drLW,
    lastWeekNum:nWeek,
    thisMonth:drTM,
    lastMonth:drLM,
  }

  var sqls = getReportSql(projectId,taskId,drTLM,drLW,drTM,drLM,retVal.tStamp)

  var dbfile = process.env.PROTTDB || 'PROTT.sqlite'

  //sqls.forEach((sql, i) => {
  for (let i = 0; i < sqls.length; i++) {
    var cmd = sqls[i]

    try{
      trace.log("open db");
      var db = await aSqlite3.open(dbfile);

      //trace.log(cmd.sql)
      const rows = await db.all(cmd.sql, []);
      await db.close()

      rows.forEach((row) => {
        //trace.log(row.data);
        //convert json string to json before sent
        row.data = JSON.parse(row.data)
        //billable from int to bool
        if (row.billable !== undefined) row.billable = (row.billable === 0) ? false : true
      });

      //trace.log(cmd.resultId);
      retVal[cmd.resultId] = rows
      //trace.log(`${cmd.resultId}:${JSON.stringify(rows)}`)
    }
    catch(e){
      trace.log("err:"+e.message);
      res.status(400).json(
        {
          stat   : "-1",
          msg    : e.message,
          tStamp : new Date().toISOString(),
        });
      return
    }
  }
  res.json(retVal);
}



function getReportSql(projectId,taskId,drTLM,drLW,drTM,drLM,nowDT){

  var retVal = [
    {
      resultId:"project",
      sql     : `
select PROJECT_ID as id, PROJECT_NAME as name, PROJECT_IS_BILLABLE as billable,
  PROJECT_CLIENT_ID client, PROJECT_DATA as data, p.CREATED_AT as startDate
from PROJECT p
inner join CLIENT c
on p.PROJECT_CLIENT_ID = c.CLIENT_ID
where p.PROJECT_ID = '${projectId}'
`
    },
    {
      resultId:"users",
      sql     : `
with taskAndTimesheet
as (
  select t.TASK_PROJECT_ID, ts.TIMESHEET_USER_ID, ts.TIMESHEET_DATE, ts.TIMESHEET_HOURS
  from TASK t
  inner join TIMESHEET ts
  on ts.TIMESHEET_TASK_ID = t.TASK_ID
  where t.TASK_PROJECT_ID = '${projectId}'
  and t.TASK_ID = '${taskId}'
)
select usr.*, tot.hours as totalHours, ifnull(lw.hours,0) as lastWeekHours,
ifnull(tm.hours,0) as thisMonthHours, ifnull(lm.hours,0) as lastMonthHours
from (
  --users active within range of this and last month
  select u.USER_ID as userId, u.USER_FIRST_NAME as firstName, u.USER_LAST_NAME as lastName,
    USER_DATA as data
  from USER u
  inner join taskAndTimesheet tts
  on u.USER_ID = tts.TIMESHEET_USER_ID
  where tts.TIMESHEET_DATE between '${drTLM.start.toISOString()}' and '${drTLM.end.toISOString()}'
  group by u.USER_ID, u.USER_FIRST_NAME, u.USER_LAST_NAME
) usr
inner join (
  -- all time users work hours
  select TIMESHEET_USER_ID as userId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet tts
  group by TASK_PROJECT_ID
) tot
on usr.userId = tot.userId
left join (
  -- last week users work hours
  select TIMESHEET_USER_ID as userId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet tts
  where tts.TIMESHEET_DATE between '${drLW.start.toISOString()}' and '${drLW.end.toISOString()}'
  group by TASK_PROJECT_ID
) lw
on usr.userId = lw.userId
left join (
  -- this month users work hours
  select TIMESHEET_USER_ID as userId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet tts
  where tts.TIMESHEET_DATE between '${drTM.start.toISOString()}' and '${drTM.end.toISOString()}'
  group by TASK_PROJECT_ID
) tm
on usr.userId = tm.userId
left join (
  -- last month users work hours
  select TIMESHEET_USER_ID as userId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet tts
  where tts.TIMESHEET_DATE between '${drLM.start.toISOString()}' and '${drLM.end.toISOString()}'
  group by TASK_PROJECT_ID
) lm
on usr.userId = lm.userId
`
    },
    {
      resultId:"task",
      sql     : `
with taskAndTimesheet
as (
  select t.TASK_ID, t.TASK_NAME, t.TASK_DATA, t.TASK_PROJECT_ID, ts.TIMESHEET_DATE, ts.TIMESHEET_HOURS
  from TASK t
  inner join TIMESHEET ts
  on ts.TIMESHEET_TASK_ID = t.TASK_ID
  where t.TASK_PROJECT_ID = '${projectId}'
  and t.TASK_ID = '${taskId}'
)

select tsk.*, ifnull(tot.hours,0) as totalHours, ifnull(lw.hours,0) as lastWeekHours,
ifnull(tm.hours,0) as thisMonthHours, ifnull(lm.hours,0) as lastMonthHours
from (
  --task data
  select p.PROJECT_ID as projectId, p.PROJECT_NAME as projectName,
    t.TASK_ID as taskId, t.TASK_NAME as taskName, t.TASK_DATA as data
  from PROJECT p
  inner join TASK t
  on p.PROJECT_ID = t.TASK_PROJECT_ID
  where p.PROJECT_ID = '${projectId}'
  and t.TASK_ID = '${taskId}'
) tsk
left join (
  -- all time task work hours
  select TASK_ID as taskId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet
  group by TASK_ID
) tot
on tsk.taskId = tot.taskId
left join (
  -- last week project work hours
  select TASK_ID as taskId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet tts
  where TIMESHEET_DATE between '${drLW.start.toISOString()}' and '${drLW.end.toISOString()}'
  group by TASK_ID
) lw
on tsk.taskId = lw.taskId
left join (
  -- this month project work hours
  select TASK_ID as taskId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet
  where TIMESHEET_DATE between '${drTM.start.toISOString()}' and '${drTM.end.toISOString()}'
  group by TASK_ID
) tm
on tsk.taskId = tm.taskId
left join (
  -- last month project work hours
  select TASK_ID as taskId, sum(TIMESHEET_HOURS) as hours
  from taskAndTimesheet tts
  where TIMESHEET_DATE between '${drLM.start.toISOString()}' and '${drLM.end.toISOString()}'
  group by TASK_ID
) lm
on tsk.taskId = lm.taskId
`
    },
  ]
  return retVal
}



function _vytvoritChybovouZpravu(msg){
  return {
    stat: "-1",
		msg: msg,
		tStamp: new Date().toISOString()
  };
}
