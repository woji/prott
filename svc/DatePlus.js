/*
 server backend DatePlus struct with functs that are not unavailable
 in pure js and used in this project

 todo:very annoying that code is so different from react DatePlus.js :((
 dont have time to consolidate


wojkowski.free@gmail.com
2022-03-16

*/


/*
returns zero based week number in year for given date
so for user interaction add 1 to return value :)
*/
exports.getWeekNumber = function(date) {

  if (!(date instanceof Date))
    day = new Date();

  else
    day = new Date(date)

  // ISO week date weeks start on Monday, so correct the day number
  var nDay = (day.getDay() + 6) % 7;

  // ISO 8601 states that week 1 is the week with the first Thursday of that year
  // Set the target date to the Thursday in the target week
  day.setDate(day.getDate() - nDay + 3);

  // Store the millisecond value of the target date
  var n1stThursday = day.valueOf();

  // Set the target to the first Thursday of the year
  // First, set the target to January 1st
  day.setMonth(0, 1);

  // Not a Thursday? Correct the date to the next Thursday
  if (day.getDay() !== 4) {
    day.setMonth(0, 1 + ((4 - day.getDay()) + 7) % 7);
  }

  // The week number is the number of weeks between the first Thursday of the year
  // and the Thursday in the target week (604800000 = 7 * 24 * 3600 * 1000)
  return 1 + Math.ceil((n1stThursday - day) / 604800000);
}



/*
https://stackoverflow.com/questions/53382465/how-can-i-determine-if-week-starts-on-monday-or-sunday-based-on-locale-in-pure-j

<firstDay day="mon" territories="001 AD AI AL AM AN AT AX AZ BA BE BG BM BN BY CH CL CM CR CY CZ DE DK EC EE ES FI FJ FO FR GB GE GF GP GR HR HU IS IT KG KZ LB LI LK LT LU LV MC MD ME MK MN MQ MY NL NO PL PT RE RO RS RU SE SI SK SM TJ TM TR UA UY UZ VA VN XK" />
<firstDay day="fri" territories="BD MV" />
<firstDay day="sat" territories="AE AF BH DJ DZ EG IQ IR JO KW LY MA OM QA SD SY" />
<firstDay day="sun" territories="AG AR AS AU BR BS BT BW BZ CA CN CO DM DO ET GT GU HK HN ID IE IL IN JM JP KE KH KR LA MH MM MO MT MX MZ NI NP NZ PA PE PH PK PR PY SA SG SV TH TN TT TW UM US VE VI WS YE ZA ZW" />
<firstDay day="sun" territories="GB" alt="variant" references="Shorter Oxford Dictionary (5th edition, 2002)" />

*/
//todo:missing bn-BD,dv-MV !! friday !!
// see https://wiki.openstreetmap.org/wiki/Nominatim/Country_Codes
//bn-BD,dv-MV
function weekStart(region, language, options) {
  const regionSat = 'AEAFBHDJDZEGIQIRJOKWLYOMQASDSY'.match(/../g);
  const regionSun = 'AGARASAUBDBRBSBTBWBZCACNCODMDOETGTGUHKHNIDILINJMJPKEKHKRLAMHMMMOMTMXMZNINPPAPEPHPKPRPTPYSASGSVTHTTTWUMUSVEVIWSYEZAZW'.match(/../g);
  const languageSat = ['ar','arq','arz','fa'];
  const languageSun = 'amasbndzengnguhehiidjajvkmknkolomhmlmrmtmyneomorpapssdsmsnsutatethtnurzhzu'.match(/../g);

  if(options && options.asSundayDiff)
    return (
      region
      ? (
        regionSun.includes(region) ? '0' :
        regionSat.includes(region) ? '-1' : '1')
      : (
        languageSun.includes(language) ? '0' :
        languageSat.includes(language) ? '-1' : '1'));

    else
      return (
        region
        ? (
          regionSun.includes(region) ? 'sun' :
          regionSat.includes(region) ? 'sat' : 'mon')
        : (
          languageSun.includes(language) ? 'sun' :
          languageSat.includes(language) ? 'sat' : 'mon'));
}



function weekStartLocale(locale, options) {
  //if(!locale) locale = navigator.language

  const parts = locale.match(/^([a-z]{2,3})(?:-([a-z]{3})(?=$|-))?(?:-([a-z]{4})(?=$|-))?(?:-([a-z]{2}|\d{3})(?=$|-))?/i);
  return weekStart(parts[4], parts[1], options);
}



//navigator.language returns cs-CZ
exports.getDateRange = function(dateRangeName, locale){
  var retVal;
  switch(dateRangeName){
  case "thisAndLastMonth":
    retVal = getThisAndLastMonthDates()
    break
  case "lastMonth":
    retVal = getLastMonthDates()
    break
  case "thisMonth":
    retVal = getThisMonthDates()
    break
  case "lastWeek":
    retVal = getLastWeek(locale)
    break

  }
  //console.log(`dateRangeName:${dateRangeName}`);
  //console.log(`retVal.start:${retVal.start.toISOString()}`);console.log(`retVal.end:${retVal.end.toISOString()}`);
  return retVal
}



function getThisAndLastMonthDates(){
  var dateRange = getCurrentMonthFirstDay()

  dateRange.start.setMonth(dateRange.start.getMonth()-1); //first day of previous month

  dateRange.end.setMonth(dateRange.end.getMonth()+1);
  dateRange.end.setDate(dateRange.end.getDate()-1) //last day of this month

  return dateRange
}



function getLastMonthDates(){
  var dateRange = getCurrentMonthFirstDay()

  dateRange.start.setMonth(dateRange.start.getMonth()-1); //first day of previous month
  dateRange.end.setDate(dateRange.end.getDate()-1) //last day of last month

  return dateRange
}



function getThisMonthDates(){
  var dateRange = getCurrentMonthFirstDay()

  dateRange.end.setMonth(dateRange.end.getMonth()+1);
  dateRange.end.setDate(dateRange.end.getDate()-1) //last day of last month

  return dateRange
}



function getCurrentMonthFirstDay(){
  var dt = new Date()

  var yr = dt.getFullYear();
  var mo = dt.getMonth();
  var dy = dt.getDay();

  var dateRange = {
    start:new Date(yr, mo, 1),
    end:new Date(yr, mo, 1)
  }

  return dateRange
}



function getLastWeek(locale){
  var dt = getStartDateOfThisWeek(locale)
  //console.log(`getLastWeek.dt:${dt.toISOString()}`);

  var dateRange = {
    start: dt,
    end: new Date(dt)
  }

  dateRange.start.setDate(dateRange.start.getDate()-7)
  dateRange.end.setDate(dateRange.end.getDate()-1)

  //console.log(`getLastWeek.dateRange.start:${dateRange.start.toISOString()}`);

  return dateRange
}



function getStartDateOfThisWeek(locale){
  var sundayDiff = weekStartLocale(locale, {asSundayDiff:true})
  var dt = new Date()
  dt.setDate(dt.getDate() + (sundayDiff - dt.getDay()))

  return dt
}



//todo:work with locales, week can start on Fri, Sat, Sun, Mon !!
exports.getStartDateOfWeek = function(w, y, locale) {
  var sundayDiff = weekStartLocale(locale, {asSundayDiff:true})

  var date = new Date(y, 0, (1 + (w * 7))); // Elle's method
  date.setDate(date.getDate() + (sundayDiff - date.getDay())); // 0 - Sunday, 1 - Monday etc

  return date
}



exports.getWeekDateRange = function(y, w, locale){

  var sundayDiff = weekStartLocale(locale, {asSundayDiff:true})

  var day = new Date(y, 0, (1 + (w * 7))); // Elle's method
  day.setDate(day.getDate() + (sundayDiff - day.getDay())); // 0 - Sunday, 1 - Monday etc

  var dateRange = {
    start: day,
    end: new Date(day)
  }

  dateRange.end.setDate(dateRange.end.getDate() + 6);

  return dateRange
}



exports.addHours = function(dateTime, h) {
  var tmp = new Date(dateTime)
  tmp.setTime(tmp.getTime() + (h*60*60*1000));
  return tmp;
}
