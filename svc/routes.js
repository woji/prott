/*
2022-02-23
wojkowski.free
*/
'use strict';

module.exports = function(app) {
  var ctrlBasicAuth = require('./ctrlBasicAuth');

  var ctrlProbe = require('./ctrlProbe');
  var ctrlTimesheet = require('./ctrlTimesheet');
  var ctrlClients = require('./ctrlClients');
  var ctrlProjects = require('./ctrlProjects');
  var ctrlTasks = require('./ctrlTasks');
  var ctrlRoles = require('./ctrlRoles');
  var ctrlUsers = require('./ctrlUsers');
  var ctrlAssignment = require('./ctrlAssignment');
  var ctrlProjectReports = require('./ctrlProjectReports');
  var ctrlTaskReports = require('./ctrlTaskReports');
  var ctrlUserReports = require('./ctrlUserReports');


  app.route('/getPing')
    .get(ctrlProbe.ping);

  app.route('/API/testDb')
    .get(ctrlProbe.testDb);

  app.route('/API/basicAuth')
    .get(ctrlBasicAuth.doBasicAuth)

  /*
  URI:
  http://192.168.56.8:8003/API/TS/2022/01/1000380?someFilter
  type:POST
  */
  app.route('/API/TS/:year/:week/:userId')
    .get(ctrlTimesheet.getUserWeekTimeSheet)
    .post(ctrlTimesheet.saveUserManDay)

  app.route('/API/clients')
    .get(ctrlClients.getClientList)
    .post(ctrlClients.modClient)

  app.route('/API/clients/:clientId/projects')
    .get(ctrlProjects.getProjectList)
    .post(ctrlProjects.modProject)

  app.route('/API/clients/:clientId/projects/:projectId/tasks')
    .get(ctrlTasks.getTaskList)
    .post(ctrlTasks.modTask)

  app.route('/API/users')
    .get(ctrlUsers.getUserList)
    .post(ctrlUsers.modUser)

  app.route('/API/users/:userId')
    .get(ctrlUsers.getUserList)

  app.route('/API/users/:userId/roles')
    .get(ctrlUsers.getUserRoles)

  app.route('/API/users/:userId/assignments')
    .get(ctrlAssignment.getAssignmentList)
    .post(ctrlAssignment.modAssignment)

  //app.route('/API/allprojects')
  //  .get(ctrlAssignment.getAllProjectList)

  app.route('/API/roles')
    .get(ctrlRoles.getRoleList)

  app.route('/API/reports/projects/:projectId')
    .get(ctrlProjectReports.getReportData)

  app.route('/API/reports/projects/:projectId/tasks/:taskId')
    .get(ctrlTaskReports.getReportData)

  app.route('/API/reports/users/:userId')
    .get(ctrlUserReports.getReportData)


  /*
  app.route('/test/:id')
    .get(ctrl.read)
    .put(ctrl.update)
    .delete(ctrl.delete);
  */
};
