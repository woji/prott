# how to run this app

I really dislike to burder dockerhub with ballast container for ...whatever. So I prefer to use pure base container
and add all necessary to it
just download this project to some suitable folder, eg. /opt/docker/PROTT

```
cd /opt
# only when not already exists :)
mkdir docker
git clone https://gitlab.com/woji/prott.git
#consider some chmods here
```

> note: download recent version of docker-compose before run

open docker-compose in your favourite editor and update:
```
    environment:
      - NODE_ENV=production
      - PROTTDB=PROTT-sampledata.sqlite
                          ^ this
```

to

```
    environment:
      - NODE_ENV=production
      - PROTTDB=PROTT.sqlite
```

to start with empty db

next, by by experience, delete node_modules folder and let it create from scratch by uncomment

and uncomment (as extra to npm run start-linux)
```
    npm i
    npm update
```

in command section

>after first run this can be commented

then only:

```
docker-compose up prott -d
```

will run only backend container service, with attached precompiled SPA from prottuidev service :)

app should be available then at:
http://[your.server.ipv4.addr]:8003/ui/index.html

> to stop container when run docker-compose without -d please press ctrl+z as stdin_open: true is set in yml

## todo:how to develop
