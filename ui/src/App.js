/*
App main

wojkowski.free@gmail.com
2022-02-3

*/
import React, { useState }  from 'react';

import {
  Route,
  Routes,
  Redirect,
  //NavLink,
  HashRouter,
  //withRouter,
  useLocation,
}                           from "react-router-dom";

//import withRouter          from "./fragment/withRouter";
import * as Colors          from '@mui/material/colors';
import {
  ThemeProvider
  , createTheme
}                           from '@mui/material/styles';

import axios                from 'axios';

import
  AppStat, {
    useTranslation
  }                         from 'fragment/AppStat'
import Auth                 from 'fragment/BasicAuth'
import Console              from "./layout/Console";
import Timesheet            from "./layout/Timesheet";
import Clients              from "./layout/Clients";
import Projects             from "./layout/Projects";
import Tasks                from "./layout/Tasks";
import Users                from "./layout/Users";
import Assignment           from "./layout/Assignment";
import UserReport           from "./layout/UserReport";
import ProjectReport        from "./layout/ProjectReport";
import TaskReport           from "./layout/TaskReport";


export default function App(props){

  console.log("App()");

  const location = useLocation()
  //first try load appstat from session storage
  const [appStat, setAppStat] = useState(AppStat.load())



  React.useEffect(() => {
    //did mount
    //console.log("App.didMount()");

    //if not available, login as guest user
    /*
    if(AppStat.userInfo === null){
      var appStat = {
        userInfo:{
          stat:"0",tStamp:"2099-12-31T00:00:00.000Z",
          user:{
            id:"0000000",firstName:"Guest",lastName:"User",employeeId:"0000000",
            email:"todo@set.mail",roles:0,data:null,
            login:"guest",password:""
          },
        },
        userRoles:{
          stat:"0",tStamp:"2099-12-31T00:00:00.000Z",
          roles:["guest"]
        }
      }
      AppStat.save(appStat)

      setAppStat(appStat)
    }
    */

    //todo:in this version force login now,
    //next version login only when...
    //userAuth()
    Auth.userAuth(appStat, hndlAuth)

  },[])


  const hndlAuth = (appStat) => {
    AppStat.save(appStat)
    setAppStat(appStat)
    loadUserRoles()
  }



  React.useEffect(() => {
    // runs on location, i.e. route, change
    //console.log('handle route change here', location)
    onRouteChanged(location)
  }, [location])


  /*
  const userAuth = () => {
    //console.log("App.userAuth()");

    var appst = appStat

    console.log(appst.userInfo);

    if(
      appst.userInfo !== null
      //nebrat data z cache starsi nez hodinu
      && Math.abs(Date.now() - Date.parse(appst.userInfo.tStamp)) / 3600000 < 24
    ){
      //user browser refresh
      console.log("cache has valid user auth data");
      return; //userInfo;
    }

    //otherwise reset and load fresh from server
    console.log("user auth required");
    appst.userInfo = null;

    //save appStat
    AppStat.save(appst)
    setAppStat(appst)

    axios.get(
      "API/basicAuth",
      //"http://localhost.kzcr.eu:8000/tls/sso-kerb/auth/getOverit.ashx"
      //for kerberos must use withCredentials
      {
        withCredentials: true,
        //, crossDomain: true, baseURL:process.env.REACT_APP_NEMO_SVC_URI
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
    .then(res => {
      console.log(res.data);
      appst.userInfo = res.data;
      AppStat.save(appst)
      setAppStat(appst)
      loadUserRoles()
    })
    .catch(error => {
      console.log(error.message);
      this.setState({ authErr: error });
    });

    return;// userInfo;
  }
  */



  const loadUserRoles = () => {

    var appst = appStat

    console.log(appst);

    if(
      appst.userRoles
      //nebrat data z cache starsi nez 24 hodin
      && Math.abs(Date.now() - Date.parse(appst.userRoles.tStamp)) / 360000 < 24
    ){
      //user browser refresh
      console.log("cache has valid user roles");
      return;
    }

    //otherwise reset and load fresh from server
    console.log("user role refresh required");
    appst.userRoles = null;

    //save appStat
    AppStat.save(appst)
    setAppStat(appst)

    //return

    axios.get(`API/users/${appst.userInfo.user.id}/roles`
    , {withCredentials: true, baseURL:process.env.REACT_APP_PROTT_SVCURI}
    )
    .then(res => {
      console.log(res.data);
      appst.userRoles = res.data;
      AppStat.save(appst)
      setAppStat(appst)
    })
    .catch(error => {
      console.log(error.message);
      //setState({ nacitaniDat: false });
    });
    return;
  }



  const appThemes = {
    grey : createTheme({
      palette: {
        primary: Colors.grey,
        secondary: Colors.green,
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
      },
      // = 0.25 * 3rem = 0.75rem = 12px
      spacing:3,
      //spacing: factor => `${0.25 * factor}rem`, // (Bootstrap strategy)

      typography: {
        fontFamily: ['"Roboto Condensed"','Roboto', 'sans-serif'],
        h5:{fontSize:'1rem',fontFamily: ['"Roboto Condensed"', 'sans-serif']},
        h6:{lineHeight:'1.5rem'},
        caption:{/*lineHeight:'0.8rem',*/fontFamily: ['"Roboto Condensed"', 'sans-serif']},
      },
      /*
        lze globalne prepsat konkretni nastaveni, ktere pouzivaji ruzne UI komponenty
      */
      shape: {
        borderRadius: 8,
      },
      /*
        lze globalne prepsat vlastnosti vybrane komponenty
      */
      components: {
        MuiPaper: {
          styleOverrides: {
            root: {
              // apply theme's border-radius instead of component's default
              xborderRadius: 8,
            },
          },
        },
      },
      /*palette: {
        primary: {
          light: '#757ce8',
          main: '#3f50b5',
          dark: '#002884',
          contrastText: '#fff',
        },
        secondary: {
          light: '#ff7961',
          main: '#f44336',
          dark: '#ba000d',
          contrastText: '#000',
        },
      },*/
    })
  }



  const [theme, setTheme] = useState(appThemes.grey)



  const onRouteChanged = (location) => {
    console.log("onRouteChanged:"+location.pathname);
    //var role = "zadatel"
    //console.log(this.props.location.pathname.split("/")[1])

    /*
    var role = (location.pathname == "/")
      ? ""
      : this.state.appStat.mapaRoli[this.props.location.pathname.split("/")[1]].role;
    */
    var theme = appThemes.grey; //default

    //todo:optimalizovat, setState jen pokud bude zmena
    if(location.pathname.includes("zasobovani"))
      theme = appThemes.green;

    else if(location.pathname.includes("konsignace") || location.pathname.includes("interniSklady"))
      theme = appThemes.blue;

    else if(location.pathname.includes("objednavky"))
      theme = appThemes.blue;

    else if(location.pathname.includes("sprava"))
      theme = appThemes.red;

    else if(location.pathname.includes("zadostPristup"))
      theme = appThemes.violet;

    setTheme(theme);
  }



  return (
    <ThemeProvider theme={theme}>
      <Routes>
        <Route exact path="/" element={<Console appStat={appStat}/>}/>
        <Route exact path="/timesheet" element={<Timesheet/>}/>
        <Route exact path="/clients" element={<Clients/>}/>
        <Route exact path="/clients/:clientId/projects" element={<Projects/>}/>
        <Route exact path="/clients/:clientId/projects/:projectId/tasks" element={<Tasks/>}/>
        <Route exact path="/clients/:clientId/projects/:projectId/tasks/:taskId/report/" element={<TaskReport/>}/>
        <Route exact path="/clients/:clientId/projects/:projectId/report" element={<ProjectReport/>}/>
        <Route exact path="/users" element={<Users/>}/>
        <Route exact path="/users/:userId/assignments" element={<Assignment/>}/>
        <Route exact path="/users/:userId/report" element={<UserReport/>}/>
      </Routes>
    </ThemeProvider>
  );

}
