/*
 AppStat struct with functs for persistency of app settings

wojkowski.free@gmail.com
2021-12-24

*/

//import axios from 'axios';
//avoid import useTranslation everywhere
import {useTranslation}    from 'fragment/i18n-fake'
import {DatePlus}          from 'fragment/DatePlus'

const STAT_NAME = "prott-appStat"

const AppStat = {
  load: function(options){
    //try load...
    var appStat = JSON.parse(sessionStorage.getItem(STAT_NAME))

    //unavailable, create new
    if(!appStat){
      appStat = {
        appName : "PROTT",
        userInfo:null,
        userRoles:null,
      }
    }
    return appStat
  },

  save: function(appStat){
    sessionStorage.setItem(
      STAT_NAME
      , JSON.stringify(appStat)
    );
  },

  //users role membership
  isMemberOf: function(appStat, roleName){
    //console.log("userRole");
    //console.log(!appStat.userRoles)
    //console.log(!(!appStat.userRoles || !appStat.userRoles.roles.includes(roleName)));
    //console.log(roleName);
    //console.log(appStat.userRoles.roles);
    //console.log(appStat.userRoles.roles.includes(roleName))
    return !(!appStat.userRoles || !appStat.userRoles.roles.includes(roleName))
  }
}


export {useTranslation}
export {DatePlus}
export default AppStat
