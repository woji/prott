/*
InfoCard for employee/project/task report
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-17
*/
import React, { useState } from "react";

import { Link } from 'react-router-dom'

import Paper           from '@mui/material/Paper';
import Box             from '@mui/material/Box';
//import Button          from '@mui/material/Button';
import { useTheme }    from '@mui/material/styles';
//import ButtonBase      from '@mui/material/ButtonBase';

import Stack               from '@mui/material/Stack';
import Divider             from '@mui/material/Divider';

import FunctionsIco        from '@mui/icons-material/Functions';
import CalendarMonthIco    from '@mui/icons-material/CalendarMonth';
import DateRangeIco        from '@mui/icons-material/DateRange';
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';
import HistoryIco          from '@mui/icons-material/History';


export default function InfoCard(props){

  console.log("InfoCard()");

  const theme = useTheme();

  //console.log(props);
  //console.log(props.project.id);
  //console.log(props.project.name);

  var title, caption, bgColor,
    totalHours, lastWeekHours,
    thisMonthHours, lastMonthHours

  //console.log(props);

  if(props.project){
    title = props.project.name
    caption = props.project.clientId
    bgColor = props.project.data.color === null ? 'rgba(255,255,255,0.4)' : props.project.data.color
    totalHours=props.project.totalHours
    lastWeekHours=props.project.lastWeekHours
    thisMonthHours=props.project.thisMonthHours
    lastMonthHours=props.project.lastMonthHours
  }
  else if(props.task) {
    title = props.task.taskName
    caption = props.task.projectName
    bgColor = props.task.data.color === null ? 'rgba(255,255,255,0.4)' : props.task.data.color
    totalHours=props.task.totalHours
    lastWeekHours=props.task.lastWeekHours
    thisMonthHours=props.task.thisMonthHours
    lastMonthHours=props.task.lastMonthHours
  }
  else {
    title = `${props.user.firstName} ${props.user.lastName}`
    caption = props.user.employeeId
    bgColor = 'rgba(255,255,255,0.4)'
    totalHours=props.user.totalHours
    lastWeekHours=props.user.lastWeekHours
    thisMonthHours=props.user.thisMonthHours
    lastMonthHours=props.user.lastMonthHours
  }


  return (
    <Paper sx={{
      p:theme.spacing(4),
      display: 'flex',flexDirection:'row',justifyContent: 'left',
    }}>
      <Stack direction="column" spacing={2}>
        <Box fontSize="h5.fontSize" color="text.primary">
          {title}
        </Box>

        <Box fontSize="caption.fontSize" color="text.disabled">
          {caption}
        </Box>

        <Stack direction="row">

          <FunctionsIco color="disabled" fontSize="small"/>
          <Box fontSize="subtitle.fontSize" color="text.primary" width={50}>
            {totalHours}
          </Box>

          <DateRangeIco color="disabled" fontSize="small"/>
          <Box fontSize="subtitle.fontSize" color="text.primary" width={50}>
            {lastWeekHours}
          </Box>
        </Stack>

        <Stack direction="row">
        <CalendarMonthIco color="disabled" fontSize="small"/>
          <Box fontSize="subtitle.fontSize" color="text.primary" width={50}>
            {thisMonthHours}
          </Box>

          <HistoryIco color="disabled" fontSize="small"/>
          <Box fontSize="subtitle.fontSize" color="text.primary" width={50}>
            {lastMonthHours}
          </Box>
        </Stack>

      </Stack>
    </Paper>
  )

}
