/*
 DatePlus struct with functs that are not unavailable
 in pure js and used in this project


wojkowski.free@gmail.com
2022-02-28

*/

function weekNumber(date) {
  if (!(date instanceof Date)) date = new Date();

  // ISO week date weeks start on Monday, so correct the day number
  var nDay = (date.getDay() + 6) % 7;

  // ISO 8601 states that week 1 is the week with the first Thursday of that year
  // Set the target date to the Thursday in the target week
  date.setDate(date.getDate() - nDay + 3);

  // Store the millisecond value of the target date
  var n1stThursday = date.valueOf();

  // Set the target to the first Thursday of the year
  // First, set the target to January 1st
  date.setMonth(0, 1);

  // Not a Thursday? Correct the date to the next Thursday
  if (date.getDay() !== 4) {
    date.setMonth(0, 1 + ((4 - date.getDay()) + 7) % 7);
  }

  // The week number is the number of weeks between the first Thursday of the year
  // and the Thursday in the target week (604800000 = 7 * 24 * 3600 * 1000)
  return 1 + Math.ceil((n1stThursday - date) / 604800000);
}



/*
https://stackoverflow.com/questions/53382465/how-can-i-determine-if-week-starts-on-monday-or-sunday-based-on-locale-in-pure-j

<firstDay day="mon" territories="001 AD AI AL AM AN AT AX AZ BA BE BG BM BN BY CH CL CM CR CY CZ DE DK EC EE ES FI FJ FO FR GB GE GF GP GR HR HU IS IT KG KZ LB LI LK LT LU LV MC MD ME MK MN MQ MY NL NO PL PT RE RO RS RU SE SI SK SM TJ TM TR UA UY UZ VA VN XK" />
<firstDay day="fri" territories="BD MV" />
<firstDay day="sat" territories="AE AF BH DJ DZ EG IQ IR JO KW LY MA OM QA SD SY" />
<firstDay day="sun" territories="AG AR AS AU BR BS BT BW BZ CA CN CO DM DO ET GT GU HK HN ID IE IL IN JM JP KE KH KR LA MH MM MO MT MX MZ NI NP NZ PA PE PH PK PR PY SA SG SV TH TN TT TW UM US VE VI WS YE ZA ZW" />
<firstDay day="sun" territories="GB" alt="variant" references="Shorter Oxford Dictionary (5th edition, 2002)" />

*/
//todo:missing bn-BD,dv-MV !! friday !!
// see https://wiki.openstreetmap.org/wiki/Nominatim/Country_Codes
//bn-BD,dv-MV
function weekStart(region, language) {
  const regionSat = 'AEAFBHDJDZEGIQIRJOKWLYOMQASDSY'.match(/../g);
  const regionSun = 'AGARASAUBDBRBSBTBWBZCACNCODMDOETGTGUHKHNIDILINJMJPKEKHKRLAMHMMMOMTMXMZNINPPAPEPHPKPRPTPYSASGSVTHTTTWUMUSVEVIWSYEZAZW'.match(/../g);
  const languageSat = ['ar','arq','arz','fa'];
  const languageSun = 'amasbndzengnguhehiidjajvkmknkolomhmlmrmtmyneomorpapssdsmsnsutatethtnurzhzu'.match(/../g);

  return (
    region ? (
      regionSun.includes(region) ? 'sun' :
      regionSat.includes(region) ? 'sat' : 'mon') : (
      languageSun.includes(language) ? 'sun' :
      languageSat.includes(language) ? 'sat' : 'mon'));
}



function weekStartLocale(locale) {
  if(!locale) locale = navigator.language

  const parts = locale.match(/^([a-z]{2,3})(?:-([a-z]{3})(?=$|-))?(?:-([a-z]{4})(?=$|-))?(?:-([a-z]{2}|\d{3})(?=$|-))?/i);
  return weekStart(parts[4], parts[1]);
}


//console.log(weekStartLocale(navigator.language));
//console.log(weekStartLocale('en'));
//console.log(weekStartLocale('en-GB'));
//console.log(weekStartLocale('ar-AE'));



function getDayName(date, locale)
{
  if(!locale) locale = navigator.language
  return date.toLocaleDateString(locale, { weekday: 'short' });
}



function getLocalWeekDayNames(locale){
  var retV = [];

  if(!locale) locale = navigator.language

  var startDay = new Date(Date.parse('2020-01-03T00:00:00')) //sure friday

  //there are 4 week start days around world (fri, sat, sun, mon)
  switch(weekStartLocale(locale)) {
  case "fri":
    break;
  case "sat":
    startDay.setDate(startDay.getDate() + 1);
    break;
  case "sun":
    startDay.setDate(startDay.getDate() + 2);
    break;
  case "mon":
    startDay.setDate(startDay.getDate() + 3);
  }

  for(var i=0;i<7;i++) {
    retV.push(getDayName(startDay, locale))
    startDay.setDate(startDay.getDate() + 1)
  }
  //console.log(retV);
  return retV

}



const DatePlus = {
  weekNo: weekNumber,
  //navigator.language returns cs-CZ
  weekStartDay: weekStartLocale,
  localWeekDayNames: getLocalWeekDayNames
}


export {DatePlus}
