/*
SnackbarPlus
createt by: wojkowski.free
created at:2022-03-28
*/
import React, { useState, useEffect } from "react";

import Button              from '@mui/material/Button';
import Snackbar            from '@mui/material/Snackbar';
import Alert               from '@mui/material/Alert';
import { useTheme }        from '@mui/material/styles';

export default function SnackbarPlus(props){

  console.log("SnackbarPlus()");

  const theme = useTheme();


  useEffect(() => {
    // did mount

  }, [])


  var actionBtn, alert, msg
  if(props.view.type==="action"){
    actionBtn=(
      <Button color="secondary" size="small" onClick={props.onAction}>
        {props.actionLabel}
      </Button>
    )
    alert=null
    msg=props.view.msg
  }
  else {
    actionBtn=null
    msg=null
    alert=(
      <Alert
        variant="filled"
        onClose={props.onClose} severity={props.view.severity}
      >
        {props.view.msg}
      </Alert>
    )
  }



  return (
    <Snackbar
      open={props.view.open} autoHideDuration={3000}
      onClose={props.onClose}
      anchorOrigin={{vertical:'bottom',horizontal:'center',}}
      action={actionBtn}
      message={msg}
    >
      {alert}
    </Snackbar>
  )

}
