/*
wojkowski.free@gmail.com
2022-01-05

fake i18n for development without installed (and being annoyed) by i18next

l10n key should be composed like:
[namespace]:key.nestedKey.nestedKey

where
namespace is filename with translation
key is react module name (eg App)
nestedKey is up to author :)

todo:still struggling whether as key separator use "·" or "." with natural keys
normal dot is very common in text, so it should be removed from natural keys
to be used as key separator

so it will looks like this in react
t("common:app.vitejte v aplikaci") //with dot
t("common:app·vitejte v aplikaci") //with middle dot
t("app·vitejte v aplikaci", { ns: 'common' })


also let the code survive this
{
  "friend_male_one": "A boyfriend",
  "friend_female_one": "A girlfriend",
  "friend_male_other": "{{count}} boyfriends",
  "friend_female_other": "{{count}} girlfriends"
}

i18next.t('friend', {context: 'male', count: 1}); // -> "A boyfriend"
i18next.t('friend', {context: 'female', count: 1}); // -> "A girlfriend"
i18next.t('friend', {context: 'male', count: 100}); // -> "100 boyfriends"
i18next.t('friend', {context: 'female', count: 100}); // -> "100 girlfriends"
*/

export function useTranslation(options) {
  //const [state, setState] = useState(initialState);

  const noopSep = '·';

  function tran(complexKey, params) {
    return complexKey.substring(complexKey.indexOf(noopSep) + 1);
  }

  const i18noop = {
    //some fake props

  }

  return [tran, i18noop];
}
