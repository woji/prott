/*
RoleList to add to Users and Tasks
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import FormGroup           from '@mui/material/FormGroup';
import FormControlLabel    from '@mui/material/FormControlLabel';
import Checkbox            from '@mui/material/Checkbox';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

import axios               from 'axios';

import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function RoleList(props){

  console.log("RoleList()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const [records,setRecords] = useState([])
  const [roles,setRoles] = useState(0)


  useEffect(() => {
    // props changed
    //console.log(props);
    if(props.value)
      setRoles(props.value)

    loadData()
  }, [props])



  const loadData = () => {
    console.log("RoleList.loadData()");
    //console.log(props);

    var qryStr = ""
    if(props.type === "projectRoles")
      qryStr = "?type=project"

    //console.log(qryStr);

    axios.get(
      `API/roles${qryStr}`,
      {
        withCredentials: true,
        //params: {logMisto, role, qr, katg},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          //refreshList(res.data, "");
          //console.log(res.data);
          setRecords(res.data.records)
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }



  const hndlChange = (e, propId) => {
    //console.log(e.target.checked);
    var tmp
    if(e.target.checked)
      tmp = parseInt(e.target.id) | roles
    else
      tmp = parseInt(e.target.id) ^ roles

    //console.log(tmp);
    setRoles(tmp)
    props.onChange(tmp)
  }

  //console.log(records);
  //console.log(roles);

  //render always no problem, just one tiny component
  return (
    <FormGroup row={true} sx={{pt:theme.spacing(5)}}>
      {
        (props.type !== "projectRoles")
          ? <FormControlLabel disabled control={<Checkbox defaultChecked={true} id={"0"} />} label={t("guest")} />
          : null
      }
      {records.filter(rec => rec.id > 0).map((rec, ix) => (
        <FormControlLabel
          key={ix}
          label={rec.name}
          control={<Checkbox id={rec.id.toString()} defaultChecked={!!(rec.id & roles)} onChange={hndlChange}/>}
        />
      ))}
    </FormGroup>
  );

}
