/*
Auth module

implement following for your organization
this version is basic auth agains PROTT backend
when auth against LDAP, there will be need for attribute mapping
and in any case, some minimum users account data must exist in PROTT.sqlite
to be able to map app roles to user


wojkowski.free@gmail.com
2022-03-23

*/


import axios                from 'axios';
//import AppStat              from 'fragment/AppStat'


function getUserAuth(appst, stateHandler) {
  //console.log("App.userAuth()");

  //var appst = AppStat.load()

  console.log(appst.userInfo);

  if(
    appst.userInfo !== null
    //nebrat data z cache starsi nez hodinu
    && Math.abs(Date.now() - Date.parse(appst.userInfo.tStamp)) / 3600000 < 24
  ){
    //user browser refresh
    console.log("cache has valid user auth data");
    return; //userInfo;
  }

  //otherwise reset and load fresh from server
  console.log("user auth required");
  appst.userInfo = null;

  //save appStat
  //AppStat.save(appst)
  //setAppStat(appst)

  axios.get(
    "API/basicAuth",
    //"http://localhost.kzcr.eu:8000/tls/sso-kerb/auth/getOverit.ashx"
    //for kerberos must use withCredentials
    {
      withCredentials: true,
      /*, crossDomain: true, baseURL:process.env.REACT_APP_NEMO_SVC_URI*/
      baseURL:process.env.REACT_APP_PROTT_SVCURI
    }
  )
  .then(res => {
    console.log(res.data);
    appst.userInfo = res.data;
    //AppStat.save(appst)
    //setAppStat(appst)
    //loadUserRoles()
    stateHandler(appst)
  })
  .catch(error => {
    console.log(error.message);
    //setState({ authErr: error });
  });

  return;// userInfo;
}



const Auth = {
  userAuth: getUserAuth,

}


export default Auth
