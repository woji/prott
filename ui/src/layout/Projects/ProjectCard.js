/*
ProjectCard for project record mgmt
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-20
*/
import React, { useState } from "react";

import { Link } from 'react-router-dom'

import Paper           from '@mui/material/Paper';
import Box             from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useTheme }    from '@mui/material/styles';
//import ButtonBase      from '@mui/material/ButtonBase';

import Card            from '@mui/material/Card';
import CardHeader      from '@mui/material/CardHeader';
import CardContent     from '@mui/material/CardContent';
import CardActions     from '@mui/material/CardActions';
import Stack           from '@mui/material/Stack';
import IconButton      from '@mui/material/IconButton';

import EditIco         from '@mui/icons-material/Edit';
import FinishIco       from '@mui/icons-material/SportsScore';
import StartIco        from '@mui/icons-material/Tour';
import CalendarIco     from '@mui/icons-material/AvTimer';
import TimelineIco     from '@mui/icons-material/Timeline';

import TimePlanIco     from '@mui/icons-material/HourglassTop';
import TimeSpentIco    from '@mui/icons-material/HourglassBottom';
import DeleteIco       from '@mui/icons-material/DeleteForever';

import {grey}          from '@mui/material/colors';

import
  AppStat, {
    useTranslation,
    DatePlus
  }                         from 'fragment/AppStat'


export default function ProjectCard(props){

  console.log("ProjectCard()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()
  //console.log(props.project.id);
  //console.log(props.project.name);

  const bgColor = ((props.project.data.color || "") === "") ? grey[100] : props.project.data.color

  const dtFmt = { year: 'numeric', month: 'long', day: 'numeric' }
  const startDate = new Date(props.project.startDate).toLocaleString(
    navigator.language, dtFmt
  )
  const endDate = new Date(props.project.endDate).toLocaleString(
    navigator.language, dtFmt
  )

  const disableEdit = !AppStat.isMemberOf(appStat, "admin")
  const disableDelete = (props.project.taskCount > 0)

  return (
    <Card sx={{
       maxWidth: 345,
       boderRadius: 8,
       width:300,height:245,
       //display: 'flex',justifyContent:'center',alignItems:'center',
       xborder:'1px solid red',
       "&:hover": {
         boxShadow: theme.shadows[3],
         transform: 'scale(1.01)',
       },
       transformOrigin: 'center center',
    }}>
      <Link
       to={props.path+"/tasks"}
       style={{ color: 'inherit', textDecoration: 'inherit'}}
      >
        <CardHeader
          title={props.project.id}
          subheader={props.project.name}
          sx={{xxborderBottom:`3px solid ${bgColor}`,bgcolor:bgColor,}}
        />
        <CardContent sx={{pt:theme.spacing(2),pl:theme.spacing(5),'&:last-child': { pb: 0 }}}>
          <Stack direction="column" spacing={theme.spacing(2)}>
            <Box fontSize="caption.fontSize" color="text.secondary">{props.project.data.desc}</Box>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <StartIco color="disabled"/>
              <Box fontSize="title.fontSize" color="text.secondary">{startDate}</Box>
            </Stack>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <FinishIco color="disabled"/>
              <Box fontSize="title.fontSize" color="text.secondary">{endDate}</Box>
            </Stack>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <TimePlanIco color="disabled"/>
              <Box fontSize="title.fontSize" color="text.secondary">{props.project.totalAllocTime}h</Box>
              <TimeSpentIco />
              <Box fontSize="title.fontSize" color="text.primary">{props.project.totalWorkTime}h</Box>
            </Stack>
          </Stack>
        </CardContent>
      </Link>
      <CardActions disableSpacing>

        <Link
          to={props.path+"/report"}
          style={{ color: 'inherit', textDecoration: 'inherit'}}
        >
          <IconButton
            aria-label="view-project-timeline"
          >
            <TimelineIco />
          </IconButton>
        </Link>

        <IconButton
          disabled={disableEdit}
          aria-label="edit-project"
          onClick={(e) => props.onEdit(props.project)}
        >
          <EditIco />
        </IconButton>

        <IconButton
          disabled={(disableEdit||disableDelete)}
          aria-label="delete-project"
          onClick={(e) => props.onDelete(props.project)}
        >
          <DeleteIco />
        </IconButton>



      </CardActions>
    </Card>

  )

}
