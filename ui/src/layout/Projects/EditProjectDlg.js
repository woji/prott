/*
AddProjectDlg to add new project
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-10
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';
import Stack               from '@mui/material/Stack';
import Switch              from '@mui/material/Switch';
import FormControlLabel    from '@mui/material/FormControlLabel';

import Radio               from '@mui/material/Radio';
import RadioGroup          from '@mui/material/RadioGroup';
import FormControl         from '@mui/material/FormControl';
import FormLabel           from '@mui/material/FormLabel';

import LocalizationProvider from '@mui/lab/LocalizationProvider';
import AdapterDateFns      from '@mui/lab/AdapterDateFns';
import DesktopDatePicker   from '@mui/lab/DesktopDatePicker';

import useMediaQuery       from '@mui/material/useMediaQuery';


/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function AddProjectDlg(props){
  console.log("AddProjectDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const recDefaults = {
    id:"", name:"",
    billable:false,
    visibility:0,data:{desc:"",color:""},
    clientId:props.client,
    startDate: new Date().toISOString(),
    endDate:"2099-12-31T00:00:00Z"
  }



  useEffect(() => {
    // on props change
    //console.log("EditTaskDlg.propsChanged()");
    //console.log(props.task)
    if(props.project)
      setState({project:props.project,mode:"replace"})
    else
      setState({project:recDefaults,mode:"add"})

  }, [props])



  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      project:recDefaults,
      mode:"add",
    }
  )



  const hndlChange = (e, propId) => {
    //console.log(e);

    propId = (propId === undefined) ? e.target.id : propId
    //console.log(propId);
    hndlPropChange(e.target.value, propId)
  }



  const hndlPropChange = (propVal, propId) => {
    const project = state.project
    if(propId.startsWith("data")){
      project.data[propId.split(".")[1]] = propVal
    }
    else
      project[propId] = propVal
    setState({project})
  }



  const modRecLbl = (state.mode === "add") ? t("Add") : t("Update")

  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="edit-project"
      maxWidth='xs'
      sx={{minWidth:480}}
    >
    <DialogTitle>{modRecLbl+t(" project for ")+props.client}</DialogTitle>
      <DialogContent>

        <DialogContentText>
          <Box component="span">{t("enter project data")}</Box>
        </DialogContentText>

        <TextField
          autoFocus={(state.mode === "add")}
          disabled={(state.mode !== "add")}
          margin="dense"
          id="id"
          label={t("project id/code")}
          fullWidth
          variant="outlined"
          value={state.project.id}
          onChange={hndlChange}
        />
        <TextField
          autoFocus={(state.mode !== "add")}
          margin="dense"
          id="name"
          label={t("project name")}
          fullWidth
          variant="outlined"
          value={state.project.name}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="data.desc"
          label={t("description")}
          fullWidth
          variant="outlined"
          value={state.project.data.desc}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="data.color"
          label={t("color")}
          fullWidth
          variant="outlined"
          value={state.project.data.color}
          onChange={hndlChange}
        />
        <FormControlLabel
          sx={{pt:theme.spacing(4)}}
          control={<Switch checked={state.project.billable} onChange={(e)=> {hndlPropChange(e.target.checked,"billable")}}/>}
          label={t("billable")}
        />

        <Stack
          sx={{pt:theme.spacing(8)}}
          direction={{ xs: 'column', sm: 'row' }}
          spacing={{ xs: 1, sm: 2, md: 4 }}
        >
          <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
            id={"startDate"}
            label="project start date"
            inputFormat="dd/MM/yyyy"
            variant="standard"
            value={state.project.startDate}
            onChange={(dt)=> {hndlPropChange(dt.toISOString(),"startDate")}}
            renderInput={(params) => <TextField {...params} />}
          />
          <DesktopDatePicker
            id={"endDate"}
            label="project end date"
            inputFormat="dd/MM/yyyy"
            variant="standard"
            value={state.project.endDate}
            onChange={(dt)=> {hndlPropChange(dt.toISOString(),"endDate")}}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </Stack>

      <FormControl
        id="fmct2"
        sx={{pt:theme.spacing(8)}}
      >
        <FormLabel>{t("visibility")}</FormLabel>
        <RadioGroup
          row
          aria-labelledby="project-visibility-selection"
          name="project-visibility"
          value={state.project.visibility}
          onChange={(e)=> {hndlChange(e,"visibility")}}
        >
          <FormControlLabel value={0} control={<Radio />} label={t("admin only")} />
          <FormControlLabel value={1} control={<Radio />} label="team only" />
          <FormControlLabel value={2} control={<Radio />} label="public" />
        </RadioGroup>
      </FormControl>

      </DialogContent>
      <DialogActions>
        <Button onClick={e => {setState({project:recDefaults,mode:"add"});props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={e => {props.onSave(state.project,state.mode)}}>{modRecLbl}</Button>
      </DialogActions>
    </Dialog>
  );

}
