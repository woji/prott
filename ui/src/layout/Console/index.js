/*
stranka main application console/dashboard to Access
various app functions
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-23
*/
import React, { useState, useEffect } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

/*
import InfoIco               from '@mui/icons-material/Info';
import ContactIco            from '@mui/icons-material/AlternateEmail';
import QaAIco                from '@mui/icons-material/ContactSupport';
import TagIco                from '@mui/icons-material/Tag';
import LockIco               from '@mui/icons-material/Lock';
*/
import axios               from 'axios';

import Toolbar             from './Toolbar'
import NaviCard            from './NaviCard'
import AboutDlg            from './AboutDlg'

import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'




export default function Console(props){

  console.log("Console()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const [dlgAboutOpen, setDlgAboutOpen] = useState(false)
  const [appStat, setAppStat] = useState()



  useEffect(() => {
    // props changed
    if(props.appStat){
      //console.log("update appStat");
      setAppStat(props.appStat)
    }

  }, [props])



  const naviMenu = [
    {
      icon:'AccessTimeIco',
      title:t('Timesheet'),subtitle:t('enter to fill timesheet for assigned project task'),
      path:'/timesheet'
    },
    {
      icon:'AccountTreeIco',
      title:t("Project's & Status"),subtitle:t('Setup clients, projects, project tasks and view work progress'),
      path:'/clients'
    },
    {
      icon:'ManageAccntsIco',
      title:t('Team & Performance'),subtitle:t('Create and manage your team, assing them roles and view work reports'),
      path:'/users'
    },
    {
      icon:'QaAIco',
      title:t('About'),subtitle:t('informations about this application'),
      path:'/',
      onClick: (e) => {setDlgAboutOpen(true)},
      onClose: (e) => {setDlgAboutOpen(false)},
    },
  ]



  return (
    <>
      <Toolbar/>
      <Container
        maxWidth="xl" sx={{
          xborder:'1px solid green',paddingTop:theme.spacing(3),
          display:'flex',justifyContent: 'center',
      }}>

        <Box>
          <Grid container spacing={6} sx={[
            {
              display: 'flex',flexDirection:'row',justifyContent: 'left',
              xborder:'1px solid blue',
              paddingBottom:6,
              maxWidth:'90vw',xmarginTop:10,
            }
          ]}>
            {naviMenu.map( (itm,ix) => (
              <Grid key={ix} item>
                <NaviCard
                  data={itm}
                />
              </Grid>
            ))}
          </Grid>
        </Box>
      </Container>
      <AboutDlg
        open={dlgAboutOpen}
        onClose={e => setDlgAboutOpen(false)}
      />
    </>
  )

}
