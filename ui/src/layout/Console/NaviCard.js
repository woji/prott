/*
mala navigacni karta s ikonkou ktera routuje na podstranky webu
vytvoril:wojkowski.free@gmail.com
vytvoreno:2021-12-27
*/
import React, { useState } from "react";

import { Link } from 'react-router-dom'

import Paper           from '@mui/material/Paper';
import Box             from '@mui/material/Box';
import Button          from '@mui/material/Button';
import { useTheme }    from '@mui/material/styles';
import ButtonBase      from '@mui/material/ButtonBase';
//mui ikonky
import InfoIco         from '@mui/icons-material/Info';
import ContactIco      from '@mui/icons-material/AlternateEmail';
import QaAIco          from '@mui/icons-material/ContactSupport';
import TagIco          from '@mui/icons-material/Tag';
import LockIco         from '@mui/icons-material/Lock';
import SmileIco        from '@mui/icons-material/SentimentSatisfiedAlt';
import AccessTimeIco   from '@mui/icons-material/AccessTimeFilled';
import ManageAccntsIco from '@mui/icons-material/ManageAccounts';
import AccountTreeIco  from '@mui/icons-material/AccountTree';
import TaskIco         from '@mui/icons-material/Task';
import AssignIndIco    from '@mui/icons-material/AssignmentInd';


export default function NaviCard(props){

  console.log("NaviCard()");

  const theme = useTheme();

  const mkIco = (icoName) => {
    const icoProps = {fontSize:'large',sx:{color:'text.secondary'}};

    switch (icoName) {
      case 'InfoIco':
        return <InfoIco {...icoProps}/>;
        break;
      case 'ContactIco':
        return <ContactIco {...icoProps}/>;
        break;
      case 'QaAIco':
        return <QaAIco {...icoProps}/>;
        break;
      case 'TagIco':
        return <TagIco {...icoProps}/>;
        break;
      case 'LockIco':
        return <LockIco {...icoProps}/>;
        break;
      case 'SmileIco':
        return <SmileIco {...icoProps}/>;
        break;
      case 'AccessTimeIco':
        return <AccessTimeIco {...icoProps}/>;
        break;
      case 'ManageAccntsIco':
        return <ManageAccntsIco {...icoProps}/>;
        break;
      case 'AccountTreeIco':
        return <AccountTreeIco {...icoProps}/>;
        break;
      case 'TaskIco':
        return <TaskIco {...icoProps}/>;
        break;
      case 'AssignIndIco':
        return <AssignIndIco {...icoProps}/>;
        break;

      default:
        return null
    }
  }



  return (
    <Link
      to={props.data.path} onClick={props.data.onClick}
    >
      <ButtonBase>
        <Paper elevation={1} sx={{
          boderRadius: 8,bgcolor:'rgba(255,255,255,0.4)',
          width:280,height:180,
          display: 'flex',justifyContent:'center',alignItems:'center',
          xborder:'1px solid red',
          "&:hover": {
            boxShadow: theme.shadows[3],
            transform: 'scale(1.01)',
          },
          transformOrigin: 'center center',
        }}>
          <Box sx={{
              width:280,height:180,
              display: 'flex',flexDirection:'column',
              justifyContent:'left',alignItems:'left',
              padding:8,
          }}>
            {mkIco(props.data.icon)}
            <Box sx={{
              display: 'flex',flexDirection:'column',
              justifyContent:'left',alignItems:'left',
              width:240,textAlign:'left'
            }}>
              <Box sx={{fontSize:'1.5rem'}}>{props.data.title}</Box>
              <Box sx={{
                fontSize:'1.0rem',
                color:'text.disabled'
              }}>
              {props.data.subtitle}</Box>
            </Box>
          </Box>
        </Paper>
      </ButtonBase>
    </Link>
  )

}
