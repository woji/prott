/*
EditClientDlg to add new project client
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';

import useMediaQuery       from '@mui/material/useMediaQuery';


/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function AboutDlg(props){

  console.log("AboutDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));



  useEffect(() => {
    // on props change
    //console.log("EditTaskDlg.propsChanged()");
    //console.log(props.task)

  }, [])



  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="dialog-about"
    >
    <DialogTitle>{t("About")}</DialogTitle>
      <DialogContent
        sx={{minWidth:320}}
      >
        <DialogContentText>
          <Box component="span">{t("PROject Time Tracker")}</Box>
        </DialogContentText>
        <Box>
          {t("version:0.0.1")}
        </Box>

      </DialogContent>
      <DialogActions>

      </DialogActions>
    </Dialog>
  );

}
