/*
EditClientDlg to add new project client
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';

import useMediaQuery       from '@mui/material/useMediaQuery';


/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function EditClientDlg(props){

  console.log("EditClientDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const recDefaults = {id:"", data:{name:"", desc:"", color:""}}



  useEffect(() => {
    // on props change
    //console.log("EditTaskDlg.propsChanged()");
    //console.log(props.client)
    if(props.client)
      setState({client:props.client,mode:"replace"})
    else
      setState({client:recDefaults,mode:"add"})

  }, [props])



  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      client:recDefaults,
      mode:"add",
    }
  )



  const hndlChange = (e, propId) => {
    //console.log(e);

    propId = (propId === undefined) ? e.target.id : propId
    //console.log(propId);
    hndlPropChange(e.target.value, propId)
  }



  const hndlPropChange = (propVal, propId) => {
    const client = state.client
    if(propId.startsWith("data")){
      client.data[propId.split(".")[1]] = propVal
    }
    else
      client[propId] = propVal

    setState({client})
  }



  const modRecLbl = (state.mode === "add") ? t("Add") : t("Update")

  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="edit-client"
      maxWidth='xs'
      sx={{minWidth:480}}
    >
    <DialogTitle>{modRecLbl+t(" Client")}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Box component="span">{t("enter client data")}</Box>
        </DialogContentText>
        <TextField
          autoFocus={(state.mode === "add")}
          disabled={(state.mode !== "add")}
          margin="dense"
          id="id"
          label={t("client id/code")}
          fullWidth
          variant="outlined"
          value={state.client.id}
          onChange={hndlChange}
        />
        <TextField
          autoFocus={(state.mode !== "add")}
          margin="dense"
          id="data.name"
          label={t("name")}
          fullWidth
          variant="outlined"
          value={state.client.data.name}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="data.desc"
          label={t("description")}
          fullWidth
          variant="outlined"
          value={state.client.data.desc}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="data.color"
          label={t("color")}
          fullWidth
          variant="outlined"
          value={state.client.data.color}
          onChange={hndlChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={e => {setState({client:recDefaults,mode:"add"});props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={e => {props.onSave(state.client,state.mode)}}>{modRecLbl}</Button>
      </DialogActions>
    </Dialog>
  );

}
