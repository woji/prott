/*
Clients view allows project client mgmt in PROTT
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
//import CssBaseline         from '@mui/material/CssBaseline';
import Fab                 from '@mui/material/Fab';
import Button              from '@mui/material/Button';
//import Snackbar            from '@mui/material/Snackbar';
import Alert               from '@mui/material/Alert';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

import AddIcon             from '@mui/icons-material/Add';
import axios               from 'axios';

import Toolbar             from './Toolbar'


import
  AppStat, {
    useTranslation,
    DatePlus
  }                        from 'fragment/AppStat'

import SnackbarPlus        from 'fragment/SnackbarPlus'

import EditClientDlg       from './EditClientDlg'
import ClientCard          from './ClientCard'


export default function Clients(props){

  console.log("Clients()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()
  //const [clientData, setClientData] = useState({id:null,name:null,desc:null})
  //const [editRecDlgOpen, setAddClientDlgOpen] = useState(false)
  //const [clients,setClients] = useState()
  //const [loading,setWaitForData] = useState(false)
  //const [snackView,setSnackStat] = useState({open:false,severity:"info",msg:""})

  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      client:{ id:"", name: "", desc: ""},
      loading:false,
      editRecDlgOpen:false,
      //type: alert/action, severity: info, warning, ...
      snackView:{type:"alert", open:false, severity:"info", msg:""},
      currRecord:undefined,
      records:[],
      deleted:[],
    }
  )


  useEffect(() => {
    // did mount

    loadData()

  }, [])



  const loadData = () => {
    console.log("Clients.loadData()");

    const userId = appStat.userInfo.employeeNumber

    axios.get(
      `API/clients`,
      {
        withCredentials: true,
        //params: {logMisto, role, qr, katg},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          //refreshList(res.data, "");
          //console.log(res.data);
          setState({records:res.data.records})
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }



  const syncItem = (item, op) => {
    //console.log("odeslatKosik()");
    //const appStat = this.state.appStat;
    //const kosikMista = this.state.kosikMista;

    if(item.id === "" && item.name === ""){
      setState({
        snackView:{open:true, severity:'warning', msg:t("can't save empty record")}
      });
      return;
    }

    //build json record modify msg
    var modMsg = {
      tStamp: new Date().toISOString(),
      op:op,
      client:item,
    };

    setState({loading:true})

    axios.post(
      `API/clients`,
      modMsg,
      {
        headers:{'content-type': 'application/json'},
        withCredentials: true, //params: {ulozitJako},
        baseURL:process.env.REACT_APP_PROTT_SVCURI,
      },
    )
    .then(res => {
      if(parseInt(res.data.stat) === 0){
        console.log("stav ok");
        //this.vypraznitKosik();
      }
      var records = state.records;
      var deleted = state.deleted;

      if(op === "add"){
        //add analytics (fresh record don't have it)
        item.lastUpdated = new Date().toISOString()
        item.projectCount=0
        item.taskCount=0
        item.userCount=0
        records.push(item)
      }

      else if(op === "delete")
        deleted = []

      setState({loading:false,editRecDlgOpen:false,records,deleted})
      //setWaitForData(false)
      /*
      this.setState({
        nacitaniDat: false, kosikOdeslan: true
        , dlgUlozitFavOpen: false, pacient: null
      });
      */
    })
    .catch(error => {
      console.log(error.message);
      setState({loading:false,editRecDlgOpen:false})
      //setWaitForData(false)
      //this.setState({ nacitaniDat: false, chybaDat: error });
    });
  }



  const hndlDelayedSync = (item, op) => {
    var deleted = state.deleted;
    var records = state.records;

    //console.log(!!zpracovaniZmen);
    if(!!state.loading)
      clearTimeout(state.loading);

    deleted.push(item);
    //warning, cant use .filter !!!!!, must splice !!!
    //records = records.filter((rec) => rec.id !== item.id)
    records.filter(
      (rec,ix,arr) => {
        if(rec.id === item.id)
          arr.splice(ix,1);

        return false;
      }
    );

    setState({
      records,deleted,loading:setTimeout(() => {syncItem(item,"delete")}, 3000),
      snackView:{type:"action", open: true, severity: 'warning', msg:t("deleted")}
    });

    //this.setState({ deleted, loading, snackMsg: opMsg });
  }



  const hndlUndo = () => {
    if(!!state.loading)
      clearTimeout(state.loading);

    var deleted = state.deleted;
    var records = state.records;

    //iterates undo buffer and puts all back int records
    deleted.forEach( () => {
      records.push(deleted.pop())
		});

    setState({
      records, deleted, loading: undefined,
      snackView:{open: false, severity: state.snackView.severity,msg:null}
    })
  }



  const closeSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setState({
      snackView:{open: false, severity: state.snackView.severity, msg:null}
    });
  };



  return (
    <>
      <Toolbar loading={state.loading}/>
      <Container
        maxWidth="xl" sx={{
          xborder:'1px solid green',paddingTop:theme.spacing(3),
          display:'flex',justifyContent: 'center',
      }}>
        <Grid container spacing={6} sx={[
          {
            display: 'flex',flexDirection:'row',justifyContent: 'left',
            xborder:'1px solid blue',
            paddingBottom:6,
            maxWidth:'90vw',xmarginTop:10,
          }
        ]}>
          {state.records.map( (rec, ix) => (
            <Grid key={ix} item>
              <ClientCard
                client={rec}
                path={`/clients/${rec.id}/projects`}
                onEdit={(currRecord)=>setState({currRecord,editRecDlgOpen:true})}
                onDelete={(item)=>hndlDelayedSync(item,"delete")}
              />
            </Grid>
          ))}
        </Grid>
        {(AppStat.isMemberOf(appStat, "admin"))
          ? <Fab
              sx={{
                position:'fixed', right:theme.spacing(4), bottom:theme.spacing(4),
                '& > *': {margin:theme.spacing(0),}
              }}
              aria-label={t("Add")}
              onClick={() => {setState({editRecDlgOpen:true,currRecord:undefined})} }
            >
              <AddIcon/>
            </Fab>
          : null
        }
      </Container>

      <EditClientDlg
        open={state.editRecDlgOpen}
        client={state.currRecord}
        onClose={e => setState({editRecDlgOpen:false})}
        onSave={syncItem}
      />

      <SnackbarPlus
        view={state.snackView}
        onClose={closeSnack}
        onAction={hndlUndo} actionLabel={t("undo")}
      />

    </>
  )

}
