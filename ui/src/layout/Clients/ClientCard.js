/*
ClientCard for project record mgmt
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-20
*/
import React, { useState } from "react";

import { Link } from 'react-router-dom'

import Paper           from '@mui/material/Paper';
import Box             from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useTheme }    from '@mui/material/styles';
//import ButtonBase      from '@mui/material/ButtonBase';

import Card            from '@mui/material/Card';
import CardHeader      from '@mui/material/CardHeader';
import CardContent     from '@mui/material/CardContent';
import CardActions     from '@mui/material/CardActions';
import Stack           from '@mui/material/Stack';
import IconButton      from '@mui/material/IconButton';

import EditIco         from '@mui/icons-material/Edit';
//import FinishIco       from '@mui/icons-material/SportsScore';
//import StartIco        from '@mui/icons-material/Tour';
//import CalendarIco     from '@mui/icons-material/CalendarMonth';
import LastUpdateIco   from '@mui/icons-material/EventRepeat';
import ProjectIco      from '@mui/icons-material/AccountTree';
import TaskIco         from '@mui/icons-material/PlaylistAddCheck';
import PersonIco       from '@mui/icons-material/Person';
import DeleteIco       from '@mui/icons-material/DeleteForever';
//import TimelineIco     from '@mui/icons-material/Timeline';

import {grey}          from '@mui/material/colors';

import
  AppStat, {
    useTranslation,
    DatePlus
  }                         from 'fragment/AppStat'


export default function ClientCard(props){

  console.log("ClientCard()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()

  //console.log(props.client);
  //console.log(props.project.name);

  const bgColor = ((props.client.data.color || "") === "") ? grey[100] : props.client.data.color

  const lastUpdated = new Date(props.client.lastUpdated).toLocaleString(
    //month: long, short, narrow
    navigator.language, { year: 'numeric', month: 'long', day: 'numeric' }
  )

  const disableEdit = !AppStat.isMemberOf(appStat, "admin")
  const disableDelete = (props.client.projectCount > 0)

  return (
    <Card sx={{
       maxWidth: 345,
       boderRadius: 8,
       width:300,height:230,
       //display: 'flex',justifyContent:'center',alignItems:'center',
       xborder:'1px solid red',
       "&:hover": {
         boxShadow: theme.shadows[3],
         transform: 'scale(1.01)',
       },
       transformOrigin: 'center center',
     }}>
       <Link
         to={props.path}
         style={{ color: 'inherit', textDecoration: 'inherit'}}
       >
        <CardHeader
          title={props.client.id}
          subheader={props.client.data.name}
          sx={{xxborderBottom:`3px solid ${bgColor}`,bgcolor:bgColor,}}
        />
        <CardContent sx={{p:theme.spacing(4),'&:last-child': { pb: 0 }}}>
          <Stack spacing={theme.spacing(2)} sx={{height:80}}>
            <Box fontSize="caption.fontSize" color="text.secondary">{props.client.data.desc}</Box>

            <Stack direction="row" spacing={theme.spacing(2)}>
              <ProjectIco />
              <Box sx={{width:60}}>{props.client.projectCount}</Box>
              <TaskIco />
              <Box sx={{width:60}}>{props.client.taskCount}</Box>
            </Stack>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <PersonIco />
              <Box sx={{width:60}}>{props.client.userCount}</Box>
              <LastUpdateIco />
              <Box fontSize="title.fontSize" color="text.secondary" sx={{width:150}}>{lastUpdated}</Box>
            </Stack>

          </Stack>
        </CardContent>
      </Link>
      <CardActions disableSpacing>

        <IconButton
          disabled={disableEdit}
          aria-label="edit-client"
          onClick={(e) => props.onEdit(props.client)}
        >
          <EditIco />
        </IconButton>
        <IconButton
          disabled={(disableEdit || disableDelete)}
          aria-label="delete-client"
          onClick={(e) => props.onDelete(props.client)}
        >
          <DeleteIco />
        </IconButton>

      </CardActions>
    </Card>

  )

}
