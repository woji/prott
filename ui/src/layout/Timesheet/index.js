/*
Timesheet view allows enter user time data to PROTT
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-24
*/
import React, { useState, useEffect } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import Fab                 from '@mui/material/Fab';
//import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

//import axios               from 'axios';
//import AccessTimeIco       from '@mui/icons-material/AccessTime';

import Toolbar             from './Toolbar'
import WeekPicker          from './WeekPicker'
import WeekTaskList        from './WeekTaskList'

import
  AppStat, {
    useTranslation,
    DatePlus
  }                        from 'fragment/AppStat'


export default function Timesheet(props){

  console.log("Timesheet()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const [weekDT, setWeekDT] = useState({week:DatePlus.weekNo(new Date()),year:new Date().getFullYear()})



  useEffect(() => {
    // did mount


  }, [])



  //console.log("Timesheet.render()");

  return (
    <>
      <Toolbar/>
      <Container
        maxWidth="xl" sx={{
          xborder:'1px solid green',paddingTop:theme.spacing(3),
          display:'flex',justifyContent: 'center',
      }}>
        <Box>
          <Box sx={[
            {
              display: 'flex',flexDirection:'row',justifyContent: 'center',
              xborder:'1px solid blue',
              paddingBottom:6,
              maxWidth:'90vw',xmarginTop:10,
            }
          ]}>
            <Box>
              <WeekPicker
                firstWeekDay={DatePlus.weekStartDay()}
                weekDT={weekDT}
                onWeekChange={setWeekDT}
              />
              <WeekTaskList sx={{pt:theme.spacing(10)}}
                weekDT={weekDT}
              />
            </Box>
          </Box>
        </Box>
        <Fab
          sx={{position: 'absolute',
            bottom: 16,
            right: 16,display:'none',
          }}
          aria-label={t("workhours")}
          color="secondary"
        >
          {"?h"}
        </Fab>
      </Container>
    </>
  )

}
