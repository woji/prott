/*
WeekTaskView view display tasks in week
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-25
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
import Stack               from '@mui/material/Stack';
import Divider             from '@mui/material/Divider';

//import TextField           from '@mui/material/TextField';
import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

import Avatar              from '@mui/material/Avatar';

import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';
import axios               from 'axios';

import
  AppStat, {
    useTranslation,
    DatePlus
  }                        from 'fragment/AppStat'

import TaskManDay        from './TaskManDay'
import TaskHoursUpdDlg     from './TaskHoursUpdDlg'


export default function WeekTaskList(props){

  console.log("WeekTaskList()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()

  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      weekData:[],
      loading:false,
      editDayDlgOpen:false,
      activeManDay:undefined,
      scrMsg:t("please wait..."),
      sumHours:0,
      weekStartDate:new Date(),
      weekEndDate:new Date(),
      //snackStat:{open:false,severity:"info",msg:""},
    }
  )



  useEffect(() => {
    // props changed
    //load this week data when start
    const now = new Date()
    loadData(props.weekDT);
  }, [props])


  const loadData = (weekDt) => {
    console.log("WeekTaskList.loadData()");

    const userId = appst.userInfo.user.id

    axios.get(
      `API/TS/${weekDt.year}/${weekDt.week}/${userId}`,
      {
        withCredentials: true,
        params: {loc:navigator.language},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          console.log(res.data);
          const weekData=res.data.records
          const sumHours=weekData.reduce((tmp, a) => tmp + a.manHours, 0);
          const weekStartDate=new Date(weekData[0].dateMD)
          const weekEndDate=new Date(weekData[weekData.length-1].dateMD)
          setState({weekData,sumHours,weekStartDate,weekEndDate})
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }


  /*
  const refreshList = (resp, msg) => {
    console.log("obnovitSeznam()");
    setWeekData(resp.data)

  }
  */



  const hndlDelayedTaskSave = (opMsg, polozka) => {
    //var undoBuf = this.state.undoBuf;

    //console.log(!!zpracovaniZmen);
    if(!!state.loading)
      clearTimeout(state.loading);

    //undoBuf.push(polozka);

    setState({loading:setTimeout(() => {saveTask()}, 3000)});

    //this.setState({ undoBuf, zpracovaniPozadavku, snackMsg: opMsg });
  }



  const saveTask = (year,weekNo,taskMD) => {
    console.log("saveTask()");
    //var undoBuf = this.state.undoBuf;
    var userId = appst.userInfo.user.id;

    var msg = {
      tStamp: new Date().toISOString()
      ,record: taskMD
      ,op:"replace"
    };

    axios.post(
      `API/TS/${year}/${weekNo}/${userId}`,
      msg,
      {
        headers:{'content-type': 'application/json'},
        withCredentials: true,
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
    .then(res => {
      //console.log(res.data);
      //const odpoved = res.data;
      //todo:kontrola
      //this.setState({zpracovaniPozadavku: undefined}); //ulozeni schvaleni
    })
    .catch(error => {
      console.log(error.message);
      //this.setState({ chybaDat: error });
    });

    //this.setState({ undoBuf: [], zpracovaniPozadavku: undefined, snackMsg :""  });
  }



  const onEnter = (e) => {
    //po stisknuti enter se spusti hledani
    //pouzije se slovo ktere je v textovem poli
    if (e.key === 'Enter') {
      console.log('enter press');
      //const hledanyText = week;

      if(
        !props.nacitaniDat
        //&& weekValueOk(weekDT.week)
      ){
        console.log(`save taskDay change:`);
        //props.onWeekChange(weekDT);
      }
      e.preventDefault();
    }
  }



  const hndlChangeMH = (taskManDay) => {
    //setWeekDT(event.target.value);
    console.log(taskManDay);
    //console.log(weekData);
    //todo:update weekData with new values

    const weekData = state.weekData
    var sumHours = state.sumHours

    weekData[taskManDay.rowid] = taskManDay
    sumHours=weekData.reduce((tmp, a) => tmp + a.manHours, 0);
    setState({editDayDlgOpen:false,weekData,sumHours})
    saveTask(props.weekDT.year, props.weekDT.week, taskManDay)
  };



  //no need for translation uses browser locales
  const wd = DatePlus.localWeekDayNames()



  const ctlLoading = (
    <Box sx={{display:'flex',marginTop:theme.spacing(5),height:'70vh',
      textAlign:'center',justifyContent:'center',
      alignItems:'center',xborder:'1px solid blue'}}
    >
      <AccessTimeIco fontSize='large' sx={{color:'text.secondary',padding:theme.spacing(2)}}/>
      <Box color="textDisabled" component="h4" sx={{
        display: 'flex',justifyContent:'center',alignItems:'center',
        height: '40vmin',width:'3hmin',minWidth:'80px',
        pointerEvents: 'none',padding:theme.spacing(2)
      }}>
        {state.scrMsg}
      </Box>
    </Box>
  )

  const dateRangeLocal = ``

  const ctlHead = (
    <>
      <Grid item xs={12}>
        <Stack
          direction="row"
          divider={<Divider orientation="vertical" flexItem />}
          spacing={15}
          sx={{pb:theme.spacing(4)}}
        >
          <Avatar sx={{ bgcolor: Colors.indigo[500],width: 60, height: 60 }}>{state.sumHours}</Avatar>
          <Stack spacing={3}>
            <Box sx={{textAlign:'center'}} fontSize="h5.fontSize" component="span" color="text.secondary">
              {state.weekStartDate.toLocaleString(
                'cz-CS', {  year: 'numeric', month: 'long', day: 'numeric' }
              )}
            </Box>
            <Box sx={{textAlign:'center'}} fontSize="h5.fontSize" component="span" color="text.secondary">
              {
              state.weekEndDate.toLocaleString(
                'cz-CS', {  year: 'numeric', month: 'long', day: 'numeric' }
              )
              }
            </Box>
          </Stack>
          <Box sx={{textAlign:'center'}} fontSize="h4.fontSize" component="p" color="text.secondary">
            {t("week")+" "+props.weekDT.week}
          </Box>
        </Stack>
      </Grid>

      <Grid item xs={4}>
        <Box sx={{textAlign:'right',pr:theme.spacing(3)}} fontSize="h5.fontSize" component="p" color="text.disabled">
        {t("task/project")}
      </Box>
      </Grid>
      {wd.map( (day,ix) => (
        <Grid key={day} item xs={1}>
          <Box sx={{textAlign:'center'}}  fontSize="h5.fontSize" component="p" color="text.disabled">{day}</Box>
        </Grid>
      ))}
    </>
  )

  //console.log("WeekTaskList.render()");

  return (
    <Box
      maxWidth="xl" sx={{
      xborder:'1px solid green',pt:theme.spacing(4),
      display:'flex',justifyContent: 'center',
    }}>
      {(state.weekData.length === 0)
      ? (ctlLoading)
      : <Grid container spacing={2} sx={[
          {
            display: 'flex',flexDirection:'row',justifyContent: 'left',
            xborder:'1px solid blue',
            paddingBottom:6,width:550,
            maxWidth:'90vw',marginTop:theme.spacing(5),
          }
        ]}>
          {ctlHead}
          {state.weekData.map( (rec,ix) => (
            ((ix % 7) === 0) //7 days
              ? <React.Fragment key={ix}>
                  <Grid item xs={4} sx={{xborder:'1px solid blue',pr:theme.spacing(3)}}>
                    <Stack spacing={1}>
                      <Box sx={{textAlign:'right'}} fontSize="h5.fontSize" component="span" color="text.primary">
                        {rec.taskName}
                      </Box>
                      <Box sx={{textAlign:'right'}} fontSize="caption.fontSize" component="span" color="text.disabled">
                        {rec.projectName}
                      </Box>
                    </Stack>
                  </Grid>
                  <Grid item xs={1}>
                    <TaskManDay
                      taskDay={rec}
                      onChange={hndlChangeMH}
                      onClick={(e) => setState({activeManDay:rec,editDayDlgOpen:true})}
                    />
                    </Grid>
                  </React.Fragment>
              : <Grid key={ix} item xs={1}>
                  <TaskManDay
                    taskDay={rec}
                    onChange={hndlChangeMH}
                    onClick={(e) => setState({activeManDay:rec,editDayDlgOpen:true})}
                  />
                </Grid>
          ))}
        </Grid>
      }
      <TaskHoursUpdDlg
        open={state.editDayDlgOpen}
        onClose={e => setState({editDayDlgOpen:false})}
        onChange={hndlChangeMH}
        taskManDay={state.activeManDay}
      />
    </Box>
  )

}
