/*
WeekPicker view allows select week
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-25
*/
import React, { useState, useEffect } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import TextField           from '@mui/material/TextField';
//import Input           from '@mui/material/Input';

/*
import InfoIco               from '@mui/icons-material/Info';
import ContactIco            from '@mui/icons-material/AlternateEmail';
import QaAIco                from '@mui/icons-material/ContactSupport';
import TagIco                from '@mui/icons-material/Tag';
import LockIco               from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';
import axios               from 'axios';

import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function WeekPicker(props){

  console.log("WeekPicker()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()


  useEffect(() => {
    // did mount

  }, [])

  const [weekDT, setWeekDT] = React.useState(props.weekDT);

  const hndlChangeWeek = (event) => {
    //console.log(event.target.value);
    weekDT.week = event.target.value
    setWeekDT(weekDT);
  };


  /*
  uzivak stisknul enter ve vyhledavacim poli
  */
  const onEnter = (e) => {
    //po stisknuti enter se spusti hledani
    //pouzije se slovo ktere je v textovem poli
    if (e.key === 'Enter') {
      //console.log('press enter');
      //const hledanyText = week;

      if(
        weekValueOk(weekDT.week)
      ){
        console.log(`load week:${weekDT.week}`);
        //console.log(weekDT);
        //setWeekDT(weekDT);
        //props.onWeekChange(weekDT);
        //nutno prepsat novou hodnotou jinak se zmena nevyvola!!
        const newVal = {week:weekDT.week,year:weekDT.year}
        props.onWeekChange(newVal);
      }
      e.preventDefault();
    }
  }


  const weekValueOk = (fieldVal) => {

    //todo:validation
    return true
  }




  if(!weekDT) return null

  return (
    <Box
      maxWidth="xl" sx={{
      xborder:'1px solid green',paddingTop:theme.spacing(3),
      display:'flex',justifyContent: 'center',
    }}>

      <Grid container spacing={6} sx={[
        {
          display: 'flex',flexDirection:'row',justifyContent: 'left',
          xborder:'1px solid blue',
          paddingBottom:6,
          maxWidth:'90vw',xmarginTop:10,
        }
      ]}>
        <Grid item xs={6}>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t("set week number")}
            type="number"
            fullWidth
            variant="standard"
            defaultValue={weekDT.week}
            onChange={hndlChangeWeek}
            onKeyPress={onEnter}
          />
        </Grid>
      </Grid>

    </Box>
  )

}
