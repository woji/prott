/*
TaskManDay control allows enter hours for task
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-25
*/
import React, { useState, useEffect } from "react";

//import Box                 from '@mui/material/Box';
//import Grid                from '@mui/material/Grid';
//import Container           from '@mui/material/Container';
//import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
//import { useTheme }        from '@mui/material/styles';
import TextField           from '@mui/material/TextField';
import Avatar              from '@mui/material/Avatar';
import ButtonBase          from '@mui/material/ButtonBase';
//import Input           from '@mui/material/Input';

import {deepOrange}        from '@mui/material/colors';
import {grey}        from '@mui/material/colors';

/*
import InfoIco               from '@mui/icons-material/Info';
import ContactIco            from '@mui/icons-material/AlternateEmail';
import QaAIco                from '@mui/icons-material/ContactSupport';
import TagIco                from '@mui/icons-material/Tag';
import LockIco               from '@mui/icons-material/Lock';
*/
//import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';
//import axios               from 'axios';

import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function TaskManDay(props){

  //console.log("TaskManDay()");

  //const theme = useTheme();
  const [t, i18n] = useTranslation()
  //const appst = AppStat.load()

  const [taskDay, setTaskDay] = React.useState(props.taskDay);

  useEffect(() => {
    // props change
    //console.log("TaskManDay.useEffect");
    setTaskDay(props.taskDay)
  }, [props])



  const hndlChangeHours = (event) => {
    //setWeekDT(event.target.value);
    console.log(event.target.value);
    const newVal = {week:event.target.value,year:taskDay.year}
    setTaskDay(newVal)
    props.onChange(taskDay);
  };



  const hourValueOk = (fieldVal) => {

    //todo:validation
    return true
  }



  if(!taskDay) return null

  const startDT = new Date(taskDay.startDate)
  const endDT = new Date(taskDay.endDate)
  const dayDT = new Date(taskDay.dateMD)

  var tColor = deepOrange[500], interactive = true

  if(startDT > dayDT || dayDT > endDT){
    tColor = grey[300]
    interactive = false
  } else if (!taskDay.billable)
    tColor = grey[500]

  //console.log(taskDay.TIMESHEET_HOURS);

  return (
    (interactive)
    ? <ButtonBase onClick={props.onClick}>
        <Avatar sx={{ bgcolor: tColor }}>{taskDay.manHours}</Avatar>
      </ButtonBase>
    : <Avatar sx={{ bgcolor: tColor }}>{taskDay.manHours}</Avatar>
  )

}
