/*
TaskHoursUpdDlg to update work hours on task on day
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-25
*/
import React, { useState, useEffect } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';
import Avatar              from '@mui/material/Avatar';
import ButtonBase          from '@mui/material/ButtonBase';

import useMediaQuery       from '@mui/material/useMediaQuery';

import {deepOrange}        from '@mui/material/colors';
/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function TaskHoursUpdDlg(props){

  console.log("TaskHoursUpdDlg()");
  //console.log(props.taskManDay);

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  const [taskManDay, setTaskManDay] = useState()

  useEffect(() => {
    // props change
    setTaskManDay(props.taskManDay)
  }, [props])


  //console.log(props.taskManDay);

  const updateData = () => {
    //setOpen(false);
    //todo:save changes

    //close dlg
    props.onClose(false)
  };


  /*
  const hndlChange = (e) => {
    //console.log(e);
    setTaskManDay(e.target.value)
  }*/



  const changeMH = (newMH) => {
    //console.log(e);
    const tmp = Object.assign({}, taskManDay);
    tmp.manHours = newMH
    setTaskManDay(tmp)
  }



  const onEnter = (e) => {

    if (e.key === 'Enter') {
      console.log('enter press');
      props.onChange(taskManDay);

      e.preventDefault();
    }
  }



  const predefinedWH = [
    0.5, 1, 2, 4, 8, 12
  ]



  //exit non initialized
  if(!taskManDay)
    return null

  //console.log(taskManDay);

  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="responsive-dialog-title"
    >
    <DialogTitle>{props.taskManDay.projectName},{props.taskManDay.taskName}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Box component="span">{props.taskManDay.dateMD}</Box>
        </DialogContentText>
        <Box sx={{pt:theme.spacing(4),pb:theme.spacing(4),minWidth:380}}>
        {predefinedWH.map( (val, ix) => (
          <ButtonBase key={ix} onClick={(e) => changeMH(val)}>
            <Avatar sx={{ bgcolor: deepOrange[500] }}>{val}</Avatar>
          </ButtonBase>
        ))}
        </Box>
        <TextField
          autoFocus
          margin="normal"
          id="name"
          label={t("your work hours")}
          type="number"
          fullWidth
          variant="outlined"
          value={taskManDay.manHours}
          onChange={(e) => changeMH(e.target.value)}
          onKeyPress={onEnter}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={e => {props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={e => {props.onChange(taskManDay)}}>{t("Ok")}</Button>
      </DialogActions>
    </Dialog>
  );

}
