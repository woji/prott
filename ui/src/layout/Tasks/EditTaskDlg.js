/*
EditTaskDlg to add new project
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-10
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';
import Stack               from '@mui/material/Stack';
import Switch              from '@mui/material/Switch';
import FormControlLabel    from '@mui/material/FormControlLabel';

import Radio               from '@mui/material/Radio';
import RadioGroup          from '@mui/material/RadioGroup';
import FormControl         from '@mui/material/FormControl';
import FormLabel           from '@mui/material/FormLabel';

import LocalizationProvider from '@mui/lab/LocalizationProvider';
import AdapterDateFns      from '@mui/lab/AdapterDateFns';
import DesktopDatePicker   from '@mui/lab/DesktopDatePicker';

import useMediaQuery       from '@mui/material/useMediaQuery';


/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'
import RoleList            from 'fragment/RoleList'


export default function EditTaskDlg(props){
  console.log("EditTaskDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const recDefaults = {
    id:"", name:"",data:{color:"",desc:""},
    projectId:props.project,
    roles:0,estimateTime:0,
    resourceRate:1.0,
    startDate: new Date().toISOString(),
    endDate:"2099-12-31T00:00:00Z"
  }



  useEffect(() => {
    // on props change
    //console.log("EditTaskDlg.propsChanged()");
    //console.log(props.task)
    if(props.task)
      setState({task:props.task,mode:"replace"})
    else
      setState({task:recDefaults,mode:"add"})

  }, [props])



  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      task:recDefaults,
      mode:"add",
    }
  )



  const hndlChange = (e, propId) => {
    //console.log(e);

    propId = (propId === undefined) ? e.target.id : propId
    //console.log(propId);
    hndlPropChange(e.target.value, propId)
  }



  const hndlPropChange = (propVal, propId) => {
    const task = state.task
    if(propId.startsWith("data")){
      task.data[propId.split(".")[1]] = propVal
    }
    else
      task[propId] = propVal

    //adjust estimate time
    if(propId === "estimateTime" || propId === "startDate" || propId === "endDate"){
      //console.log("adjust estimate");
      var estTime = validateDuration(state.task.startDate, state.task.endDate)
      if(task.estimateTime > estTime)
        task.estimateTime=estTime
    }

    setState({task})
  }



  const validateDuration = (dtStart, dtEnd) => {
    var start = new Date(dtStart)
    var end = new Date(dtEnd)

    //console.log(start instanceof Date);

    if(!(start instanceof Date && start instanceof Date))
      return 1000000 //return value that changes nothing :)

    var days = Math.ceil( (end.getTime() - start.getTime()) / (1000 * 3600 * 24));
    //console.log(days);
    //https://stackoverflow.com/questions/4228356/how-to-perform-an-integer-division-and-separately-get-the-remainder-in-javascr
    var weeks = (days/7|0) //number of weeks
    var extraDays = days%7
    if(extraDays === 6) extraDays=5 //remove weekend day, very aproximate !!
    //console.log(weeks);
    days = weeks * 5 // just work days (without holidays)
    //console.log(days);
    return days+extraDays
  }



  const modRecLbl = (state.mode === "add") ? t("Add") : t("Update")


  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby={t("edit-task")}
      maxWidth='xs'
      sx={{minWidth:480}}
    >
    <DialogTitle>{modRecLbl+t(" task ")+props.project}</DialogTitle>
      <DialogContent>

        <DialogContentText>
          <Box component="span">{t("enter task data")}</Box>
        </DialogContentText>

        <TextField
          autoFocus={(state.mode === "add")}
          disabled={(state.mode !== "add")}
          margin="dense"
          id="id"
          label={t("task id/code")}
          fullWidth
          variant="outlined"
          value={state.task.id}
          onChange={hndlChange}
        />
        <TextField
          autoFocus={(state.mode !== "add")}
          margin="dense"
          id="name"
          label={t("task name")}
          fullWidth
          variant="outlined"
          value={state.task.name}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="data.desc"
          label={t("description")}
          fullWidth
          variant="outlined"
          value={state.task.data.desc}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="data.color"
          label={t("color")}
          fullWidth
          variant="outlined"
          value={state.task.data.color}
          onChange={hndlChange}
        />
        <Stack
          sx={{pt:theme.spacing(4),pb:theme.spacing(4)}}
          direction={{ xs: 'column', sm: 'row' }}
          spacing={{ xs: 1, sm: 2, md: 4 }}
        >
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DesktopDatePicker
              id={"startDate"}
              label="task start date"
              inputFormat="dd/MM/yyyy"
              variant="standard"
              value={state.task.startDate}
              onChange={(dt)=> {hndlPropChange(dt.toISOString(),"startDate")}}
              renderInput={(params) => <TextField {...params} />}
            />
            <DesktopDatePicker
              id={"endDate"}
              label="task end date"
              inputFormat="dd/MM/yyyy"
              variant="standard"
              value={state.task.endDate}
              onChange={(dt)=> {hndlPropChange(dt.toISOString(),"endDate")}}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
        </Stack>
        <TextField
          margin="dense"
          id="estimateTime"
          label={t("estimated workdays")}
          fullWidth
          variant="outlined"
          value={state.task.estimateTime}
          onChange={hndlChange}
        />
        <TextField
          margin="dense"
          id="resourceRate"
          label={t("estimated task resource assignment")}
          fullWidth
          variant="outlined"
          value={state.task.resourceRate}
          onChange={hndlChange}
        />
        <RoleList
          type="projectRoles"
          onChange={(val) => hndlPropChange(val,"roles")}
          value={state.task.roles}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={e => {setState({task:recDefaults,mode:"add"});props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={e => {props.onSave(state.task,state.mode)}}>{modRecLbl}</Button>
      </DialogActions>
    </Dialog>
  );

}
