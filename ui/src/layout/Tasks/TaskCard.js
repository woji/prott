/*
TaskCard for project record mgmt
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState } from "react";

import { Link } from 'react-router-dom'

import Paper           from '@mui/material/Paper';
import Box             from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useTheme }    from '@mui/material/styles';
//import ButtonBase      from '@mui/material/ButtonBase';

import Card            from '@mui/material/Card';
import CardHeader      from '@mui/material/CardHeader';
import CardContent     from '@mui/material/CardContent';
import CardActions     from '@mui/material/CardActions';
import Stack           from '@mui/material/Stack';
import IconButton      from '@mui/material/IconButton';

import EditIco         from '@mui/icons-material/Edit';
import FinishIco       from '@mui/icons-material/SportsScore';
import StartIco        from '@mui/icons-material/Tour';
import CalendarIco     from '@mui/icons-material/AvTimer';
import DeleteIco       from '@mui/icons-material/DeleteForever';

import {grey}          from '@mui/material/colors';

import
  AppStat, {
    useTranslation,
    DatePlus
  }                         from 'fragment/AppStat'

export default function TaskCard(props){

  console.log("TaskCard()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()

  //console.log(props.task.id);
  //console.log(props.task.name);

  const bgColor = ((props.task.data.color || "") === "") ? grey[100] : props.task.data.color

  const dtFmt = { year: 'numeric', month: 'long', day: 'numeric' }
  const startDate = new Date(props.task.startDate).toLocaleString(
    navigator.language, dtFmt
  )
  const endDate = new Date(props.task.endDate).toLocaleString(
    navigator.language, dtFmt
  )

  const disableEdit = !AppStat.isMemberOf(appStat, "admin")
  const disableDelete = (props.task.totalWorkTime > 0)

  return (
    <Card sx={{
       maxWidth: 345,
       boderRadius: 8,
       width:300,height:240,
       //display: 'flex',justifyContent:'center',alignItems:'center',
       xborder:'1px solid red',
       "&:hover": {
         boxShadow: theme.shadows[3],
         transform: 'scale(1.01)',
       },
       transformOrigin: 'center center',
    }}>
      <Link
         to={props.path+"/report"}
         style={{ color: 'inherit', textDecoration: 'inherit'}}
      >
        <CardHeader
          title={props.task.id}
          subheader={props.task.name}
          sx={{xxborderBottom:`3px solid ${bgColor}`,bgcolor:bgColor,}}
        />
        <CardContent sx={{pb:theme.spacing(0),pt:theme.spacing(4),'&:last-child': { pb: 0 }}}>
          <Stack direction="column" spacing={theme.spacing(2)}>
            <Box>{props.task.desc}</Box>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <StartIco />
              <Box fontSize="title.fontSize" color="text.secondary">{startDate}</Box>
            </Stack>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <FinishIco />
              <Box fontSize="title.fontSize" color="text.secondary">{endDate}</Box>
            </Stack>
            <Stack direction="row" spacing={theme.spacing(2)}>
              <CalendarIco />
              <Box>{props.task.estimateTime}</Box>
            </Stack>
          </Stack>
        </CardContent>
      </Link>
      <CardActions disableSpacing>

        <IconButton
          disabled={disableEdit}
          aria-label="edit-task"
          onClick={(e) => props.onEdit(props.task)}
        >
          <EditIco />
        </IconButton>

        <IconButton
          disabled={(disableEdit||disableDelete)}
          aria-label="edit-task"
          onClick={(e) => props.onDelete(props.task)}
        >
          <DeleteIco />
        </IconButton>

      </CardActions>
    </Card>

  )

}
