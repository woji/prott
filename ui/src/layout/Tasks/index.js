/*
Tasks view allows project task mgmt in PROTT
created by:wojkowski.free@gmail.com
created at:2022-03-10
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
import Fab                 from '@mui/material/Fab';

//import Snackbar            from '@mui/material/Snackbar';
import Alert               from '@mui/material/Alert';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

import {
  useLocation,
}                           from "react-router-dom";

/*
import InfoIco               from '@mui/icons-material/Info';
import ContactIco            from '@mui/icons-material/AlternateEmail';
import QaAIco                from '@mui/icons-material/ContactSupport';
import TagIco                from '@mui/icons-material/Tag';
import LockIco               from '@mui/icons-material/Lock';
*/
import AddIcon             from '@mui/icons-material/Add';
import axios               from 'axios';

import Toolbar             from './Toolbar'


import
  AppStat, {
    useTranslation,
    DatePlus
  }                        from 'fragment/AppStat'

import SnackbarPlus        from 'fragment/SnackbarPlus'
import EditTaskDlg         from './EditTaskDlg'
import TaskCard            from './TaskCard'


export default function Tasks(props){

  console.log("Tasks()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()

  const location = useLocation()

  //console.log(location);
  const clientId = location.pathname.split('/')[2]
  const projectId = location.pathname.split('/')[4]
  //console.log(clientId);console.log(projectId);

  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      //project:{ id:"", name: "", desc: ""},
      loading:false,
      editRecDlgOpen:false,
      snackView:{type:"alert", open:false, severity:"info", msg:""},
      records:[],
      currRecord:undefined,
      deleted:[],
    }
  )


  useEffect(() => {
    // did mount

    loadData()

  }, [])



  const loadData = () => {
    console.log("Tasks.loadData()");

    const userId = appStat.userInfo.employeeNumber

    axios.get(
      `API/clients/${clientId}/projects/${projectId}/tasks`,
      {
        withCredentials: true,
        //params: {logMisto, role, qr, katg},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          //refreshList(res.data, "");
          console.log(res.data);
          setState({records:res.data.records})
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }



  const syncItem = (item, op) => {
    console.log("syncItem()");
    console.log(item);

    if(item.id === "" && item.name === ""){
      setState({
        snackView:{type:"alert", open:true, severity:'warning', msg:t("can't save empty record")}
      });
      return;
    }

    //build json record modify msg
    var modMsg = {
      tStamp: new Date().toISOString(),
      op:op,
      project:item,
    };
    //console.log(JSON.stringify(kosik));

    //setWaitForData(true)
    setState({loading:true})

    axios.post(
      `API/clients/${clientId}/projects/${projectId}/tasks`,
      modMsg,
      {
        headers:{'content-type': 'application/json'},
        withCredentials: true, //params: {ulozitJako},
        baseURL:process.env.REACT_APP_PROTT_SVCURI,
      },
    )
    .then(res => {
      if(parseInt(res.data.stat) === 0){
        console.log("stat ok");
      }
      else {
        setState({
          snackView:{type:"alert", open:true, severity:'warning', msg:res.data.msg}
        });
      }
      var records = state.records;
      var deleted = state.deleted;
      if(op === "add")
        records.push(item)

      else if(op === "delete")
        deleted = []

      setState({loading:false,editRecDlgOpen:false,records,deleted})
      //setWaitForData(false)
      /*
      this.setState({
        nacitaniDat: false, kosikOdeslan: true
        , dlgUlozitFavOpen: false, pacient: null
      });
      */
    })
    .catch(error => {
      console.log(error.message);
      setState({
        loading:false,
        snackView:{type:"alert", open:true, severity:'warning', msg:error.message}
      });

      //setWaitForData(false)
      //this.setState({ nacitaniDat: false, chybaDat: error });
    });
  }



  const hndlDelayedSync = (item, op) => {
    var deleted = state.deleted;
    var records = state.records;

    //console.log(!!zpracovaniZmen);
    if(!!state.loading)
      clearTimeout(state.loading);

    deleted.push(item);
    //warning, cant use .filter !!!!!, must splice !!!
    //records = records.filter((rec) => rec.id !== item.id)
    records.filter(
      (rec,ix,arr) => {
        if(rec.id === item.id)
          arr.splice(ix,1);

        return false;
      }
    );

    setState({
      records,deleted,loading:setTimeout(() => {syncItem(item,"delete")}, 3000),
      snackView:{type:"action", open: true, severity: 'warning', msg:t("deleted")}
    });

    //this.setState({ deleted, loading, snackMsg: opMsg });
  }



  const hndlUndo = () => {
    if(!!state.loading)
      clearTimeout(state.loading);

    var deleted = state.deleted;
    var records = state.records;

    //iterates undo buffer and puts all back int records
    deleted.forEach( () => {
      records.push(deleted.pop())
    });

    setState({
      records, deleted, loading: undefined,
      snackView:{open: false, severity: state.snackView.severity,msg:null}
    })
  }



  const closeSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setState({
      snackView:{open: false, severity: state.snackView.severity}
    });
  };



  return (
    <>
      <Toolbar loading={state.loading} projectId={projectId}/>
      <Container
        maxWidth="xl" sx={{
          xborder:'1px solid green',paddingTop:theme.spacing(3),
          display:'flex',justifyContent: 'center',
      }}>
        <Grid container spacing={6} sx={[
          {
            display: 'flex',flexDirection:'row',justifyContent: 'left',
            xborder:'1px solid blue',
            paddingBottom:6,
            maxWidth:'90vw',xmarginTop:10,
          }
        ]}>
          {state.records.map( (rec, ix) => (
            <Grid key={ix} item>
              <TaskCard
                task={rec}
                path={`/clients/${clientId}/projects/${projectId}/tasks/${rec.id}`}
                onEdit={(currRecord)=>setState({currRecord,editRecDlgOpen:true})}
                onDelete={(item)=>hndlDelayedSync(item,"delete")}
              />
            </Grid>
          ))}
        </Grid>
        {(AppStat.isMemberOf(appStat, "admin"))
          ? <Fab
              sx={{
                position:'fixed', right:theme.spacing(4), bottom:theme.spacing(4),
                '& > *': {
                  margin:theme.spacing(0),
              }}}
              aria-label={t("Add")}
              onClick={() => {setState({editRecDlgOpen:true,currRecord:undefined})} }
            >
              <AddIcon/>
            </Fab>
          : null
        }
      </Container>

      <EditTaskDlg
        open={state.editRecDlgOpen}
        project={projectId}
        onClose={e => setState({editRecDlgOpen:false,currRecord:undefined})}
        onSave={syncItem}
        task={state.currRecord}
      />

      <SnackbarPlus
        view={state.snackView}
        onClose={closeSnack}
        onAction={hndlUndo} actionLabel={t("undo")}
      />

    </>
  )

}
