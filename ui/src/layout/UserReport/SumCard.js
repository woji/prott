/*
SumCard for employee project participation report
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-17
*/
import React, { useState, useEffect } from "react";

import { Link } from 'react-router-dom'

import Avatar              from '@mui/material/Avatar';
import Box                 from '@mui/material/Box';
//import Button              from '@mui/material/Button';
import { useTheme }        from '@mui/material/styles';
//import ButtonBase          from '@mui/material/ButtonBase';

import Stack               from '@mui/material/Stack';
import Divider             from '@mui/material/Divider';

import FunctionsIco        from '@mui/icons-material/Functions';
import CalendarMonthIco    from '@mui/icons-material/CalendarMonth';
import DateRangeIco        from '@mui/icons-material/DateRange';
//import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';
import HistoryIco          from '@mui/icons-material/History';

import * as Colors         from '@mui/material/colors';


export default function SumCard(props){

  console.log("SumCard()");

  const theme = useTheme();

  const [dataSum, setDataSum] = useState({total:0,prod:0,nonProd:0})
  const [colors, setColors] = useState(
    {
      total:Colors.indigo[500],
      prod:Colors.green[700],
      nonProd:Colors.deepOrange[500]
    }
  )

  useEffect(() => {
    // props changed
    if(props.data){
      var propName, colors
      switch(props.icon){
      case "CalendarMonthIco":
        propName="thisMonthHours"
        colors={total:Colors.indigo[500],prod:Colors.green[700],nonProd:Colors.deepOrange[500]}
        break
      case "HistoryIco":
        propName="lastMonthHours"
        colors = {total:Colors.grey[700],prod:Colors.grey[600],nonProd:Colors.grey[500]}
        break
      case "DateRangeIco":
        propName="lastWeekHours"
        colors = {total:Colors.grey[500],prod:Colors.grey[400],nonProd:Colors.grey[300]}
        break
      }

      const dataSum={
        total:props.data.reduce((tmp, a) => tmp + a[propName], 0),
        prod:props.data.filter( tmp => tmp.billable).reduce((tmp, a) => tmp + a[propName], 0),
        nonProd:props.data.filter( tmp => !tmp.billable).reduce((tmp, a) => tmp + a[propName], 0),
      }
      setDataSum(dataSum)
      setColors(colors)
    }

  }, [props])



  const mkIco = (icoName) => {
    const icoProps = {fontSize:'large',sx:{color:'text.secondary'}};

    switch (icoName) {
      case 'CalendarMonthIco':
        return <CalendarMonthIco {...icoProps}/>;
        break;
      case 'DateRangeIco':
        return <DateRangeIco {...icoProps}/>;
        break;
      case 'HistoryIco':
        return <HistoryIco {...icoProps}/>;
        break;

      default:
        return null
    }
  }

  //console.log(dataSum);
  //console.log(colors);

  //if(!colors || !dataSum)
  //  return null


  return (
    <Stack direction="row" spacing={4}>

      {mkIco(props.icon)}

      <Stack direction="column" sx={{pl:theme.spacing(2),pr:theme.spacing(2)}}>

        <Box fontSize="caption.fontSize" color="text.secondary" sx={{width:150}}>
          {props.dates.start}
        </Box>

        <Box fontSize="caption.fontSize" color="text.secondary" >
          {props.dates.end}
        </Box>
      </Stack>

      <Avatar sx={{ bgcolor: colors.total}}>{dataSum.total}</Avatar>
      <Avatar sx={{ bgcolor: colors.prod}}>{dataSum.prod}</Avatar>
      <Avatar sx={{ bgcolor: colors.nonProd}}>{dataSum.nonProd}</Avatar>

    </Stack>

  )

}
