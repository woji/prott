/*
Task report view to view task progress
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-31
*/
import React, { useState, useEffect, useReducer } from "react";

//import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
//import CssBaseline         from '@mui/material/CssBaseline';
//import Fab                 from '@mui/material/Fab';

import Snackbar            from '@mui/material/Snackbar';
import Alert               from '@mui/material/Alert';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

//import AddIcon             from '@mui/icons-material/Add';

import {
  useLocation,
}                           from "react-router-dom";

//import axios               from 'axios';

import Toolbar             from './Toolbar'
import Report              from './Report'

import
  AppStat, {
    useTranslation,
  }                        from 'fragment/AppStat'


export default function TaskReport(props){

  console.log("TaskReport()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const location = useLocation()
  //const userId = location.pathname.split('/')[2]

  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      //client:{ id:"", name: "", desc: ""},
      loading:false,
      snackStat:{open:false,severity:"info",msg:""},
      projectId:location.pathname.split('/')[4],
      taskId:location.pathname.split('/')[6],
      //projects:[],tasks:[],
    }
  )



  useEffect(() => {
    // did mount

    //loadData()

  }, [])



  const closeSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setState({
      snackStat:{open: false, severity: state.snackStat.severity}
    });
  };

  /*
  const updateWeek = (weekData) => {
    console.log(weekData);
    setWeekDT(weekData);

  }*/

  return (
    <>
      <Toolbar loading={state.loading} taskId={state.taskId}/>
      <Container
        maxWidth="xl" sx={{
          xborder:'1px solid green',paddingTop:theme.spacing(12),
          display:'flex',justifyContent: 'center',
      }}>
        <Grid container spacing={6} sx={[
          {
            display: 'flex',flexDirection:'row',justifyContent: 'left',
            xborder:'1px solid blue',
            paddingBottom:6,
            maxWidth:'90vw',xmarginTop:10,
          }
        ]}>
          <Report projectId={state.projectId} taskId={state.taskId}/>
        </Grid>

      </Container>

      <Snackbar
        open={state.snackStat.open} autoHideDuration={3000}
        onClose={closeSnack}
        anchorOrigin={{vertical:'bottom',horizontal:'center',}}
      >
        <Alert
          variant="filled"
          onClose={closeSnack} severity={state.snackStat.severity}
        >
          {state.snackStat.msg}
        </Alert>
      </Snackbar>

    </>
  )

}
