/*
WeekTaskView view display tasks in week
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-02-25
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Paper           from '@mui/material/Paper';
//import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
import Stack               from '@mui/material/Stack';
import Divider             from '@mui/material/Divider';
import Switch              from '@mui/material/Switch';
import FormControlLabel    from '@mui/material/FormControlLabel';

//import TextField           from '@mui/material/TextField';
import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

//import Avatar              from '@mui/material/Avatar';

import FunctionsIco        from '@mui/icons-material/Functions';
import CalendarMonthIco    from '@mui/icons-material/CalendarMonth';
import DateRangeIco        from '@mui/icons-material/DateRange';
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';
import HistoryIco          from '@mui/icons-material/History';
//import PersonIco           from '@mui/icons-material/Person';
import axios               from 'axios';

import
  AppStat, {
    useTranslation,
    DatePlus
  }                        from 'fragment/AppStat'
import InfoCard            from 'fragment/InfoCard'

import SumCard             from './SumCard'


export default function Report(props){

  console.log("Report()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()

  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      reportData:undefined,
      fmtReportDT:undefined,
      loading:false,
      scrMsg:t("please wait..."),
      //snackStat:{open:false,severity:"info",msg:""},
    }
  )



  useEffect(() => {
    // props changed
    //load this week data when start
    const now = new Date()
    loadData(props.weekDT);
  }, [props])



  const loadData = (weekDt) => {
    console.log("ReportLoadData()");

    //const userId = appst.userInfo.employeeNumber

    axios.get(
      `/API/reports/projects/${props.projectId}/tasks/${props.taskId}`,
      {
        withCredentials: true,
        params: {loc:navigator.language},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          console.log(res.data);
          const reportData=res.data
          const fmtReportDT=formatDates(reportData.usedDates)
          //add project start to now
          fmtReportDT["total"]={
            start:doFmtDT(reportData.project[0].startDate, navigator.language, {  year: 'numeric', month: 'long', day: 'numeric' }),
            end:doFmtDT(reportData.tStamp, navigator.language, {  year: 'numeric', month: 'long', day: 'numeric' })
          }
          setState({reportData,fmtReportDT})
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }



  const formatDates = (reportDateRanges) => {
    var loc = navigator.language
    var fmtOpt = {  year: 'numeric', month: 'long', day: 'numeric' }
    var reportDates = {
      thisAndLastMonth:{
        start:doFmtDT(reportDateRanges.thisAndLastMonth.start, loc, fmtOpt),
        end:doFmtDT(reportDateRanges.thisAndLastMonth.end, loc, fmtOpt)
      },
      lastWeek:{
        start:doFmtDT(reportDateRanges.lastWeek.start, loc, fmtOpt),
        end:doFmtDT(reportDateRanges.lastWeek.end, loc, fmtOpt)
      },
      thisMonth:{
        start:doFmtDT(reportDateRanges.thisMonth.start, loc, fmtOpt),
        end:doFmtDT(reportDateRanges.thisMonth.end, loc, fmtOpt)
      },
      lastMonth:{
        start:doFmtDT(reportDateRanges.lastMonth.start, loc, fmtOpt),
        end:doFmtDT(reportDateRanges.lastMonth.end, loc, fmtOpt)
      },
    }

    return reportDates
  }

  const doFmtDT = (dateISO,locale, options) => {
    return new Date(dateISO).toLocaleString(locale, options)
  }



  const ctlLoading = (
    <Box sx={{display:'flex',marginTop:theme.spacing(4),height:'70vh',
      textAlign:'center',justifyContent:'center',
      alignItems:'center',xborder:'1px solid blue'}}
    >
      <AccessTimeIco fontSize='large' sx={{color:'text.secondary',padding:theme.spacing(2)}}/>
      <Box color="textDisabled" component="h4" sx={{
        display: 'flex',justifyContent:'center',alignItems:'center',
        height: '40vmin',width:'3hmin',minWidth:'80px',
        pointerEvents: 'none',padding:theme.spacing(2)
      }}>
        {state.scrMsg}
      </Box>
    </Box>
  )



  return (
    (!state.reportData)
    ? (ctlLoading)
    : <>
      <Box
        sx={{
        xborder:'1px solid green',p:theme.spacing(4),
        display:'flex',flexDirection:'column',justifyContent: 'start',
      }}>
        <Stack direction="column" spacing={2}>
          <Box fontSize="h4.fontSize" color="text.primary" component="p" sx={{m:0}}>
            {state.reportData.task[0].taskId}
          </Box>
          <Box fontSize="subtitle1.fontSize" color="text.primary" component="p" sx={{m:0}}>
            {state.reportData.task[0].taskName}
          </Box>
          <Stack direction="row" spacing={4}>
            <Box fontSize="subtitle1.fontSize" color="text.secondary" component="p" sx={{m:0,pt:theme.spacing(3)}}>
              {state.reportData.project[0].name}
            </Box>
            <FormControlLabel
              sx={{m:0,pt:theme.spacing(0)}}
              control={<Switch disabled checked={state.reportData.project[0].billable}/>}
              label={t("billable")}
              labelPlacement="start"
            />
          </Stack>
          <SumCard icon="FunctionsIco" data={state.reportData.task} dates={state.fmtReportDT.total}/>
          <SumCard icon="DateRangeIco" data={state.reportData.task} dates={state.fmtReportDT.lastWeek}/>
          <SumCard icon="CalendarMonthIco" data={state.reportData.task} dates={state.fmtReportDT.thisMonth}/>
          <SumCard icon="HistoryIco" data={state.reportData.task} dates={state.fmtReportDT.lastMonth}/>

        </Stack>
      </Box>

      <Box
        sx={{
        xborder:'1px solid green',pt:theme.spacing(4),
        display:'flex',flexDirection:'column',justifyContent: 'center',
      }}>
        <Box fontSize="h6.fontSize" color="text.primary" component="p" sx={{m:0}}>
          {t("user participation")}
        </Box>
        <Grid container spacing={5} sx={[
          {
            display: 'flex',flexDirection:'row',justifyContent: 'left',
            xborder:'1px solid blue',
            paddingBottom:6,width:550,
            maxWidth:'90vw',marginTop:theme.spacing(4),
          }
        ]}>
          {state.reportData.users.map( (itm, ix) => (
            <Grid item key={ix}><InfoCard user={itm}/></Grid>
          ))}
        </Grid>
      </Box>
    </>
  )

}
