/*
EditUserDlg to add new project client
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
//import Grid                from '@mui/material/Grid';
//import Container           from '@mui/material/Container';
//import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';
import IconButton          from '@mui/material/IconButton';

import OutlinedInput       from '@mui/material/OutlinedInput';
import InputLabel          from '@mui/material/InputLabel';
import InputAdornment      from '@mui/material/InputAdornment';
//import FormHelperText from '@mui/material/FormHelperText';
import FormControl         from '@mui/material/FormControl';

import Visibility          from '@mui/icons-material/Visibility';
import VisibilityOff       from '@mui/icons-material/VisibilityOff';

import useMediaQuery       from '@mui/material/useMediaQuery';


/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
//import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'
import RoleList            from 'fragment/RoleList'


export default function EditUserDlg(props){

  console.log("EditUserDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  //const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const recDefaults = {
    id:"", firstName: "", lastName: "",
    employeeId:"", email:"",roles:0,
    login:"",password:"startuj2022",
    authToken:"ABRAKADABRA"
  }



  useEffect(() => {
    // props changed
    if(props.user)
      setState({user:props.user,mode:"replace"})
    else
      setState({user:recDefaults,mode:"add"})

  }, [props])



  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      user:recDefaults,
      mode:"add",
      showPassword:false,
    }
  )



  const hndlChange = (e, propId) => {
    //console.log(e);

    propId = (propId === undefined) ? e.target.id : propId
    //console.log(propId);
    hndlPropChange(e.target.value, propId)
  }



  const hndlPropChange = (propVal, propId) => {
    const user = state.user
    user[propId] = propVal
    setState({user})
  }



  const hndlShowPassword = () => {
    setState({showPassword: !state.showPassword})
  };

  const hndlMouseDownPassword = (event) => {
    event.preventDefault();
  };



  const modRecLbl = (state.mode === "add") ? t("Add") : t("Update")

  //console.log(fullScreen);

  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="add-user"
      maxWidth="xs"
      sx={{minWidth:480}}
    >
    <DialogTitle>{modRecLbl + t(" user")}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Box component="span">{t("enter employee data")}</Box>
        </DialogContentText>
        <TextField
          autoFocus={(state.mode === "add")}
          disabled={(state.mode !== "add")}
          fullWidth margin="dense" variant="outlined"
          id="id" value={state.user.id}
          label={t("user id (eg. employeeId)")}
          onChange={hndlChange}
        />
        <TextField
          autoFocus={(state.mode !== "add")}
          fullWidth margin="dense" variant="outlined"
          id="firstName" value={state.user.firstName}
          label={t("first name")}
          onChange={hndlChange}
        />
        <TextField
          fullWidth margin="dense" variant="outlined"
          id="lastName" value={state.user.lastName}
          label={t("last name")}
          onChange={hndlChange}
        />

        <TextField
          fullWidth margin="dense" variant="outlined"
          id="login" value={state.user.login}
          label={t("login name")}
          onChange={hndlChange}
        />

        <FormControl fullWidth margin="dense"  variant="outlined">
          <InputLabel htmlFor="user-password">{t("password")}</InputLabel>
          <OutlinedInput
            id="password" value={state.user.password}
            type={state.showPassword ? 'text' : 'password'}
            onChange={hndlChange}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={hndlShowPassword}
                  onMouseDown={hndlMouseDownPassword}
                  edge="end"
                >
                  {state.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label={t("password")}
          />
        </FormControl>

        <TextField
          fullWidth margin="dense" variant="outlined"
          id="employeeId" value={state.user.employeeId}
          label={t("employeeId")}
          onChange={hndlChange}
        />
        <TextField
          fullWidth margin="dense" variant="outlined"
          id="email" value={state.user.email}
          label={t("e-mail")}
          onChange={hndlChange}
        />
        <RoleList
          onChange={(val) => hndlPropChange(val,"roles")}
          value={state.user.roles}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={e => {setState({user:recDefaults,mode:"add"});props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={e => {props.onSave(state.user,state.mode)}}>{modRecLbl}</Button>
      </DialogActions>
    </Dialog>
  );

}
