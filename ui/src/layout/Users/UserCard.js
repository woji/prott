/*
UserCard for client record mgmt
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React from "react";

import { Link } from 'react-router-dom'

//import Paper           from '@mui/material/Paper';
import Box             from '@mui/material/Box';
//import Button from '@mui/material/Button';
import { useTheme }    from '@mui/material/styles';
//import ButtonBase      from '@mui/material/ButtonBase';

import Card            from '@mui/material/Card';
import CardHeader      from '@mui/material/CardHeader';
import CardContent     from '@mui/material/CardContent';
import CardActions     from '@mui/material/CardActions';
import Stack           from '@mui/material/Stack';
import IconButton      from '@mui/material/IconButton';

import EditIco         from '@mui/icons-material/Edit';
import EmailIco        from '@mui/icons-material/Email';
import PermIdentityIco from '@mui/icons-material/PermIdentity';
import TimelineIco     from '@mui/icons-material/Timeline';
import DeleteIco       from '@mui/icons-material/DeleteForever';

import {grey}          from '@mui/material/colors';

import
  AppStat, {
    useTranslation,
    DatePlus
  }                         from 'fragment/AppStat'


export default function UserCard(props){

  console.log("UserCard()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()

  //console.log(props.user.id);
  //console.log(props.user.lastName);

  const disableEdit = !AppStat.isMemberOf(appStat, "admin")
  const disableDelete = (props.user.isUsed)

  return (
    <Card sx={{
       maxWidth: 345,
       boderRadius: 8,
       width:300,height:230,
       //display: 'flex',justifyContent:'center',alignItems:'center',
       xborder:'1px solid red',
       "&:hover": {
         boxShadow: theme.shadows[3],
         transform: 'scale(1.01)',
       },
       transformOrigin: 'center center',
    }}>
      <Link
        to={`/users/${props.user.id}/assignments`}
        style={{ color: 'inherit', textDecoration: 'inherit'}}
      >
        <CardHeader
         title={props.user.firstName+" "+props.user.lastName}
         subheader={props.user.id}
         sx={{xxborderBottom:`3px solid ${grey[100]}`,bgcolor:grey[100],}}
        />
        <CardContent>
          <Stack direction="column" spacing={theme.spacing(2)}>

              <Stack direction="row" spacing={theme.spacing(2)}>
                <EmailIco />
                <Box>{props.user.email}</Box>
              </Stack>

              <Stack direction="row" spacing={theme.spacing(2)}>
                <PermIdentityIco />
                <Box>{props.user.employeeId}</Box>
              </Stack>

          </Stack>
        </CardContent>
      </Link>
      <CardActions disableSpacing>

        <Link
          to={`/users/${props.user.id}/report`}
          style={{ color: 'inherit', textDecoration: 'inherit'}}
        >
          <IconButton aria-label="view-user-report">
            <TimelineIco />
          </IconButton>
        </Link>

        <IconButton
          disabled={disableEdit}
          aria-label="edit-user"
          onClick={(e) => props.onEdit(props.user)}
        >
          <EditIco />
        </IconButton>

        <IconButton
          disabled={(disableEdit || disableDelete)}
          aria-label="delete-user"
          onClick={(e) => props.onDelete(props.user)}
        >
          <DeleteIco />
        </IconButton>

      </CardActions>
    </Card>
  )
}
