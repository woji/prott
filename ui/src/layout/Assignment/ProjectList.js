/*
Roles to add to User
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-09
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Chip                from '@mui/material/Chip';
import Grid                from '@mui/material/Grid';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

//import axios               from 'axios';

import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'



export default function ProjectList(props){

  console.log("ProjectList()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const [records,setRecords] = useState([])
  const [projects,setProjects] = useState({})
  const appStat = AppStat.load()


  useEffect(() => {
    // did mount
    //loadData()
    if(props.data)
      setRecords(props.data)

  }, [props])



  const tglSelected = (id) => {
    //console.log(id);

    const tmp = Object.assign({}, projects);
    if(tmp[id])
      delete tmp[id]
    else
      tmp[id]=1

    //console.log(tmp);
    setProjects(tmp)
    if(props.onChange) props.onChange(tmp)
  }



  const onDelete = (project) => {
    var tmp = records
    project.assigned=false
    setRecords(tmp)
    props.onDelete({userId:props.userId,projects:[project.projectId]})
  }


  //console.log(props.view);

  const filtered = (props.view === "assigned")
    ? records.filter( rec => rec.assigned === true)
    : records.filter( rec => rec.assigned === false )

  //console.log(filtered);
  const disabled = !AppStat.isMemberOf(appStat, "admin")

  //render always no problem, just one tiny component
  return (
    <Grid container spacing={5} sx={[
      {
        display: 'flex',flexDirection:'row',justifyContent: 'left',
        xborder:'1px solid blue',
        paddingBottom:6,
        marginTop:theme.spacing(4),
      }
    ]}>
      {filtered.map((rec, ix) => (
        <Grid item key={ix}>
        {(props.action === "delete" && !rec.inUse)
        ? <Chip
            key={ix}
            id={rec.projectId}
            disabled={disabled}
            label={rec.projectId}
            onDelete={(e)=> {onDelete(rec)}}
          />
        : <Chip
            key={ix}
            id={rec.projectId}
            disabled={disabled}
            label={rec.projectId}
            color={projects[rec.projectId] ? "secondary" : "info"}
            onClick={(e)=> {tglSelected(rec.projectId)}}
          />}
        </Grid>
      ))}
    </Grid>
  );

}
