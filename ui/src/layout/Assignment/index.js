/*
Assignment view allows project task assignment to users in PROTT
created by:wojkowski.free@gmail.com
created at:2022-03-11
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
import Grid                from '@mui/material/Grid';
import Container           from '@mui/material/Container';
import CssBaseline         from '@mui/material/CssBaseline';
import Fab                 from '@mui/material/Fab';
import Stack           from '@mui/material/Stack';
import Snackbar            from '@mui/material/Snackbar';
import Alert               from '@mui/material/Alert';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';

import {
  useLocation,
}                           from "react-router-dom";

/*
import InfoIco               from '@mui/icons-material/Info';
import ContactIco            from '@mui/icons-material/AlternateEmail';
import QaAIco                from '@mui/icons-material/ContactSupport';
import TagIco                from '@mui/icons-material/Tag';
import LockIco               from '@mui/icons-material/Lock';
*/
import AddIcon             from '@mui/icons-material/Add';
import axios               from 'axios';

import Toolbar             from './Toolbar'


import
  AppStat, {
    useTranslation,
    DatePlus
  }                         from 'fragment/AppStat'

import EditAssignmentDlg        from './EditAssignmentDlg'
//import AssignmentCard          from './AssignmentCard'
import ProjectList        from './ProjectList'

export default function Assignment(props){

  console.log("Assignment()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appStat = AppStat.load()

  const location = useLocation()

  //console.log(location.pathname);
  const userId = location.pathname.split('/')[2]
  //console.log(userId)

  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      //project:{ id:"", name: "", desc: ""},
      loading:false,
      editRecDlgOpen:false,
      snackStat:{open:false,severity:"info",msg:""},
      userInfo:undefined,
      records:[],
    }
  )


  useEffect(() => {
    // did mount

    loadUserInfo()
    loadData()

  }, [])



  const loadUserInfo = () => {
    console.log("Assignment.loadUserInfo()");

    //const userId = appStat.userInfo.employeeNumber

    axios.get(
      `API/users/${userId}`,
      {
        withCredentials: true,
        //params: {logMisto, role, qr, katg},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          //refreshList(res.data, "");
          console.log(res.data);
          setState({userInfo:res.data.records[0]})
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }



  const loadData = () => {
    console.log("Assignment.loadData()");

    //const userId = appStat.userInfo.employeeNumber

    axios.get(
      `API/users/${userId}/assignments`,
      {
        withCredentials: true,
        //params: {logMisto, role, qr, katg},
        baseURL:process.env.REACT_APP_PROTT_SVCURI
      }
    )
      .then(res => {
        if(parseInt(res.data.stat) === 0){
          //refreshList(res.data, "");
          //console.log(res.data);
          setState({records:res.data.records})
        }
        //this.setState({ nacitaniDat: false });
      })
      .catch(error => {
        console.log(error.message);
        //this.setState({ nacitaniDat: false });
      });
  }



  const editAssignment = (assgData, op) => {
    console.log("editAssignment()");
    console.log(assgData);

    if(assgData.userId === "" && assgData.projectId === ""){
      setState({
        snackStat:{open:true, severity:'warning', msg:t("prazdný záznam nelze odeslat")}
      });
      return;
    }

    //build json record modify msg
    var modMsg = {
      tStamp: new Date().toISOString(),
      op:op,
      assignment:assgData,
    };
    //console.log(JSON.stringify(kosik));

    //setWaitForData(true)
    setState({loading:true})

    axios.post(
      `API/users/${userId}/assignments`,
      modMsg,
      {
        headers:{'content-type': 'application/json'},
        withCredentials: true, //params: {ulozitJako},
        baseURL:process.env.REACT_APP_PROTT_SVCURI,
      },
    )
    .then(res => {
      if(parseInt(res.data.stat) === 0){
        console.log("stav ok");
        //this.vypraznitKosik();
      }
      const records = state.records;

      records.forEach((item, i) => {
        if(assgData.projects.includes(item.projectId))
          item.assigned = (op === "add") ? true : false
      });

      //const tmp = Object.assign({}, records)

      setState({loading:false,editRecDlgOpen:false,records})
      //setWaitForData(false)
      /*
      this.setState({
        nacitaniDat: false, kosikOdeslan: true
        , dlgUlozitFavOpen: false, pacient: null
      });
      */
    })
    .catch(error => {
      console.log(error.message);
      setState({
        loading:false,
        snackStat:{open:true, severity:'warning', msg:error.message}
      });

      //setWaitForData(false)
      //this.setState({ nacitaniDat: false, chybaDat: error });
    });
  }



  const closeSnack = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setState({
      snackStat:{open: false, severity: state.snackStat.severity}
    });
  };



  /*
  const updateWeek = (weekData) => {
    console.log(weekData);
    setWeekDT(weekData);

  }*/

  //console.log("Assignments.render()");

  return (
    <>
      <Toolbar loading={state.loading}/>

      <Container
        maxWidth="xl" sx={{
          xborder:'1px solid green',pt:theme.spacing(3),pl:theme.spacing(10),
          display:'flex',justifyContent: 'start',
      }}>

        <Box
          sx={{
          xborder:'1px solid green',p:theme.spacing(4),
          display:'flex',flexDirection:'column',justifyContent: 'center',
        }}>
          <Stack direction="column" spacing={theme.spacing(2)}>
            {(state.userInfo)
              ? <Box fontSize="h4.fontSize" color="text.primary" component="p" sx={{m:0,pb:theme.spacing(4)}}>
                  {`${state.userInfo.firstName} ${state.userInfo.lastName}`}
                </Box>
              : null
            }

          </Stack>
          <ProjectList
            data={state.records}
            view="assigned"
            action="delete"
            userId={userId}
            onDelete={(selection) => editAssignment(selection,"delete")}
          />
        </Box>

        {(AppStat.isMemberOf(appStat, "admin"))
          ? <Fab
              sx={{position: 'absolute',
                bottom: 16,
                right: 16,
              }}
              aria-label={t("Add")}
              onClick={() => {setState({editRecDlgOpen:true})} }
            >
            <AddIcon/>
          </Fab>
          : null
        }
      </Container>

      <EditAssignmentDlg
        data={state.records}
        open={state.editRecDlgOpen}
        onClose={e => setState({editRecDlgOpen:false})}
        userId={userId}
        onAdd={editAssignment}
      />

      <Snackbar
        open={state.snackStat.open} autoHideDuration={3000}
        onClose={closeSnack}
        anchorOrigin={{vertical:'bottom',horizontal:'center',}}
      >
        <Alert
          variant="filled"
          onClose={closeSnack} severity={state.snackStat.severity}
        >
          {state.snackStat.msg}
        </Alert>
      </Snackbar>

    </>
  )

}
