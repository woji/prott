/*
EditAssignmentDlg to add new project
vytvoril:wojkowski.free@gmail.com
vytvoreno:2022-03-10
*/
import React, { useState, useEffect, useReducer } from "react";

import Box                 from '@mui/material/Box';
//import Grid                from '@mui/material/Grid';
//import Container           from '@mui/material/Container';
//import CssBaseline         from '@mui/material/CssBaseline';
//import * as Colors         from '@mui/material/colors';
//import { keyframes }       from '@mui/system';
import { useTheme }        from '@mui/material/styles';
import Dialog              from '@mui/material/Dialog';

import DialogTitle         from '@mui/material/DialogTitle';
import DialogContent       from '@mui/material/DialogContent';
import DialogContentText   from '@mui/material/DialogContentText';
import TextField           from '@mui/material/TextField';
import DialogActions       from '@mui/material/DialogActions';
import Button              from '@mui/material/Button';
//import Stack               from '@mui/material/Stack';
//import Switch              from '@mui/material/Switch';
//import FormControlLabel    from '@mui/material/FormControlLabel';

//import Radio               from '@mui/material/Radio';
//import RadioGroup          from '@mui/material/RadioGroup';
//import FormControl         from '@mui/material/FormControl';
//import FormLabel           from '@mui/material/FormLabel';

import LocalizationProvider from '@mui/lab/LocalizationProvider';
//import AdapterDateFns      from '@mui/lab/AdapterDateFns';
//import DesktopDatePicker   from '@mui/lab/DesktopDatePicker';

import useMediaQuery       from '@mui/material/useMediaQuery';


/*
import InfoIco             from '@mui/icons-material/Info';
import ContactIco          from '@mui/icons-material/AlternateEmail';
import QaAIco              from '@mui/icons-material/ContactSupport';
import TagIco              from '@mui/icons-material/Tag';
import LockIco             from '@mui/icons-material/Lock';
*/
import AccessTimeIco       from '@mui/icons-material/AccessTimeFilled';


import
  AppStat, {
    useTranslation
  }                        from 'fragment/AppStat'

import ProjectList        from './ProjectList'


export default function EditAssignmentDlg(props){
  console.log("EditAssignmentDlg()");

  const theme = useTheme();
  const [t, i18n] = useTranslation()
  const appst = AppStat.load()
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  const assignmentDefaults = {
    userId:props.userId, projects:[]
  }

  useEffect(() => {
    // did mount

  }, [])



  //https://stackoverflow.com/questions/53574614/multiple-calls-to-state-updater-from-usestate-in-component-causes-multiple-re-re
  const [state, setState] = useReducer(
    (state, newState) => ({...state, ...newState}),
    {
      assignment:assignmentDefaults,
      selection:{}
    }
  )



  const updTasks = (selection) => {
    setState({selection})
  }



  const hndlAdd = (e) => {
    console.log("hndlAdd");
    //console.log(state.selection);
    state.assignment.projects=[];
    Object.keys(state.selection).forEach( function(key,index) {
      // key: the name of the object key
      // index: the ordinal position of the key within the object
      state.assignment.projects.push(key)
    });
    //console.log(state.assignment);
    props.onAdd(state.assignment, 'add')
  }



  //render always no problem, just one tiny component
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.onClose}
      aria-labelledby={t("assign-project")}
      maxWidth='xs'
      sx={{minWidth:480}}
    >
    <DialogTitle>{t("assign project to ")+props.userId}</DialogTitle>
      <DialogContent>

        <DialogContentText>
          <Box component="span">{t("select projects")}</Box>
        </DialogContentText>

        <ProjectList
          data={props.data}
          view="unassigned"
          action="append"
          userId={props.userId}
          onChange={(selection) => setState({selection})}
        />

      </DialogContent>
      <DialogActions>
        <Button onClick={e => {setState({assignment:assignmentDefaults});props.onClose(false)}}>{t("Cancel")}</Button>
        <Button onClick={hndlAdd}>{t("Add")}</Button>
      </DialogActions>
    </Dialog>
  );

}
