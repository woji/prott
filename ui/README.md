# UI - app user interface

current state:

- implemented timesheet fill
- implemented add/edit/delete for client/project/task/user
- user report
- project report

missing features:

- project visibility, now all projects are visible to all users
- project burndown chart
- week number + - selector in timesheet
- L10N to at least Czech
- select date for user & project report (now fixed to current)
